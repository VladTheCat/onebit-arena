Player = {}
Player.img = createImage({
	"--------",
	"--00----",
	"-00-0---",
	"000000--",
	"000000--",
	"-00-0---",
	"--00----",
	"--------"
})
Player.imgSphere = createImage({
	"--------",
	"---00---",
	"--0--0--",
	"-0-00-0-",
	"-0-00-0-",
	"--0--0--",
	"---00---",
	"--------"
})

function Player.set(x, y)
	player = {}
	player.actor = "player"
	player.hp = 100
	player.hpPrev = 100
	player.x = x
	player.y = y
	player.hs = 0
	player.vs = 0
	player.spd = 200
	player.update = Player.update
	player.draw = Player.draw
	player.angle = 0
	player.hited = false
	player.dead = false
	player.deadTimer = 0
	player.visible = true
	player.skill = 100
	player.radius = 12
	player.buff = nil
	player.buffTime = 0
	player.solid = true
	Quad.addActor(player)
	table.insert(activities, player)
end

function Player.update(act, dt)
	
	player.hpPrev = player.hp
	if not player.dead then
		Quad.addActor(player)
		local skillprev = player.skill
		player.skill = player.skill + 4 * dt

		if skillprev < 100 and player.skill >= 100 then
			SndBuff:play()
		end
		player.buffTime = math.max(player.buffTime - dt, -1)
		if player.buff then
			player.buff.update(dt)
			if player.buffTime <= 0 then
				player.buff.destroy()
				player.buff = nil
				SndDBuff:play()
			end
		end

			
		player.hp = math.min(math.max(-1, player.hp), 100)
		player.skill = math.min(math.max(0, player.skill), 100)

		local dx, dy = 0, 0
		if kb.isDown("w") then dy = dy - 1 end
		if kb.isDown("s") then dy = dy + 1 end
		if kb.isDown("a") then dx = dx - 1 end
		if kb.isDown("d") then dx = dx + 1 end
		local angle = math.atan2(dy, dx)
		player.hs = player.spd * math.cos(angle)
		player.vs = player.spd * math.sin(angle)
		if dx == 0 then player.hs = 0 end
		if dy == 0 then player.vs = 0 end

		if dx ~= 0 or dy ~= 0 then
			ActorMove(player, player.hs, player.vs, true)
		end

		if mousePressed("l") then
			disk.throw(player.x, player.y, player.angle)
		end
		if mousePressed("r") then
			if player.skill >= 100 then
				if disk.throwed then
					if disk.action ~= "frisbee" then
						disk.permanentHome = true
						disk.canCatch = true
						player.skill = 0
						SndDBuff:play()
						if not ShowedTutor.skill then
							ShowTutorText(TutorText.skill)
							if not ShowedTutor.frisbee2 then
								ShowTutorText(TutorText.frisbee1)
								ShowedTutor.frisbee1 = true
								ShowedTutor.frisbee2 = true
							end
							ShowedTutor.skill = true
						end
					end
				else
					if disk.action == "frisbee" then
						disk.throw(player.x, player.y, player.angle)
						disk.skilled = true
						player.skill = 0
						if not ShowedTutor.skill then
							ShowTutorText(TutorText.skill)
							ShowedTutor.skill = true
						end
					end
				end
			end
		end

		if not disk.throwed and GameMode == "freeplay" then
			if mousePressed("wu") then
				DiskState = DiskState - 1
				if DiskState < 1 then
					DiskState = #DiskStates
				end
				disk.init(DiskStates[DiskState])
			elseif mousePressed("wd") then
				DiskState = DiskState + 1
				if DiskState > #DiskStates then
					DiskState = 1
				end
				disk.init(DiskStates[DiskState])
			end
		end

		player.angle = slowDir(player.angle, math.atan2(my - 300, mx - 400), pi * 4 * dt)
		player.hited = false

		if PlayerImmortal then player.hp = 100 end
		if player.hp <= 0 then
			if player.buff then
				player.buff.destroy()
				player.buff = nil
				SndDBuff:play()
			end
			player.dead = true
			Music[currentTrack]:stop()
			SndDead:play()
		end
		if disk.action == "frisbee" then
			if disk.comboTime then
				disk.comboTime = disk.comboTime - dt
				if disk.comboTime <= 0 then
					disk.combo = 0
				end
			end
		end
	else
		player.deadTimer = player.deadTimer + dt
		player.visible = true
		if math.floor((player.deadTimer * 10) % 2) == 0 then
			player.visible = false
		end
		if player.deadTimer > 2 then
			state = State.translate
			State.endScreen.load(GameTable)
			print(GameTable)
			state.load(State.game, State.endScreen)
			SndGO:play()
			player.deadTimer = 0
			GAMEOVER = true
		end
	end
	player.hp = math.min(math.max(-1, player.hp), 100)
end

function Player.draw(act)
	if player.visible then
		gr.draw(Player.img, player.x, player.y, player.angle, sizeK, sizeK, 3, 4)
	end
end

function Player.buff(buff, duration)
	if player.buff then
		player.buff.destroy()
	end
	player.buff = Player.Buff[buff]
	player.buff.load()
	player.buffTime = duration
	player.buffMaxTime = duration
	SndBuff:play()
end

Player.Buff = {}
Player.Buff.Img = {}
local bi = Player.Buff.Img
bi["speedup"] = createImage({
	"---0--",
	"---00-",
	"0-0000",
	"0-0000",
	"---00-",
	"---0--"
	})
bi["regen"] = createImage({
	"--00--",
	"--00--",
	"000000",
	"000000",
	"--00--",
	"--00--"
	})
bi["skill"] = createImage({
	"000000",
	"0-----",
	"0--000",
	"000--0",
	"-----0",
	"000000"
	})
bi["invinc"] = createImage({
	"0----0",
	"000000",
	"000000",
	"-0000-",
	"-0000-",
	"--00--"
	})
bi["time"] = createImage({
	"000000",
	"-0--0",
	"--00--",
	"--00--",
	"-0--0",
	"000000"
	})
bi["noclip"] = createImage({
	"---000",
	"-00--0",
	"-00--0",
	"-00--0",
	"-00--0",
	"---000"
	})

Player.Buff["speedup"] = {}
local buff = Player.Buff["speedup"]
buff.img = bi["speedup"]
function buff.load()
	player.spd = player.spd + 100
end

function buff.destroy()
	player.spd = player.spd - 100
end

function buff.update(dt)
end

Player.Buff["regen"] = {}
buff = Player.Buff["regen"]
buff.img = bi["regen"]

function buff.load()
end
function buff.update(dt)
	player.hp = player.hp + 5 * dt
end
function buff.destroy()
end

Player.Buff["skill"] = {}
buff = Player.Buff["skill"]
buff.img = bi["skill"]

function buff.load()
end
function buff.update(dt)
	player.skill = player.skill + 50 * dt
end
function buff.destroy()
end

Player.Buff["invinc"] = {}
buff = Player.Buff["invinc"]
buff.img = bi["invinc"]

function buff.load()
	buff.hp = player.hp
end
function buff.update(dt)
	if player.hp < buff.hp then
		player.hp = buff.hp
	end
end
function buff.destroy()
end

Player.Buff["time"] = {}
local buff = Player.Buff["time"]
buff.img = bi["time"]
function buff.load()
	TimeK = 0.5
end
function buff.destroy()
	TimeK = 1
end
function buff.update(dt)
end

Player.Buff["noclip"] = {}
local buff = Player.Buff["noclip"]
buff.img = bi["noclip"]
function buff.load()
	player.solid = false
	_visibleTimer = 0
end

function buff.destroy()
	player.solid = true
	player.visible = true
end

function buff.update(dt)
	_visibleTimer = _visibleTimer + dt
	if math.floor((_visibleTimer * 10)) % 2 == 1 then
		player.visible = false
	else
		player.visible = true
	end
end