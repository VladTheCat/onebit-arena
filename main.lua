SESSION = os.time()

DATE = os.date("!*t", SESSION)

DATES = string.format("[[[Date: %d-%d-%dTime: %d:%d:%d]]]", DATE.day, DATE.month, DATE.year, DATE.hour, DATE.min, DATE.sec)

sin = math.sin
cos = math.cos
pi = math.pi

gr = love.graphics
kb = love.keyboard
tm = love.timer


Ver = "2.0"

love.mouse.setVisible(false)

http = require "socket.http"


DiskStates = {"disk", "boomerang", "frisbee", "chakram"}
DiskState = 1
DiskTables = {61008, 43173, 61007, 61009}

Modes = {"classic", "hardcore", "freeplay", "time"}
ModeName = {"classic", "hardcore", "freeplay", "time survival"}
ModeTables = {["hardcore"] = 66451, ["freeplay"] = 66453, ["time"] = 67223}


require "debugm"
require "inifile"
require "highscores"
require "state"
require "imagegen"
require "tilemap"
require "player"
require "pickup"
require "enemy"
require "disk"
require "boss"

GJ = require "gamejolt"
require "GJLocal"
Quad = require "quad"

GJ.init(41484, "ba8407fd822a8fba259c82fdb1cb4c4b")

GJSession = love.thread.newThread("GJSessionThread.lua")
GJSession:start()

ww, wh = love.window.getWidth(), love.window.getHeight()

math.randomseed(os.time())

keysPressed = { }
keysReleased = { }

buttonsPressed = { }
buttonsReleased = { }

audio = "sfx/"
backm = "bgm/"

Music = {}
MusicBoss = {}
for i = 1, 7 do
	Music[i] = love.audio.newSource(backm.."#"..i..".mp3", "stream")
end
for i = 1, 5 do
	Music[i+7] = love.audio.newSource(backm.."BOSS"..i..".mp3", "stream")
end
currentTrack = math.random(7)

SndHit = love.audio.newSource(audio.."hit.ogg", "static")
SndScore = love.audio.newSource(audio.."scoreUp.ogg", "static")
SndStuck = love.audio.newSource(audio.."stuck.ogg", "static")
SndClick = love.audio.newSource(audio.."click.ogg", "static")
SndGO = love.audio.newSource(audio.."GameOver.ogg", "static")
SndThrow = love.audio.newSource(audio.."throw.ogg", "static")
SndDead = love.audio.newSource(audio.."dead.ogg", "static")
SndGS = love.audio.newSource(audio.."GameStart.ogg", "static")
SndBuff = love.audio.newSource(audio.."buff.ogg", "static")
SndDBuff = love.audio.newSource(audio.."debuff.ogg", "static")
SndExp = love.audio.newSource(audio.."explosion.ogg", "static")
SndHide = love.audio.newSource(audio.."hide.ogg", "static")
SndShow = love.audio.newSource(audio.."show.ogg", "static")
scoreFontAr = {
	". .0.000. . . .   .   .   .  0.   . 0.0 .00.00.0.00.00000.000. 0 .000.000.0 0.000.000.000.000.000.000.00 .000.00 .000.000.000.0 0.000. 0.0 0.0  .0   0.0  0.000.000.000 .000.000.000.0 0.0 0.0   0.0 0.0 0.000.   .0  .   .  0.   . 00.   .0  . 0 . 0 .0  .00 .   .   .   .   .   .   .   . 0 .   .   .   .   .   .   .  0.    0 0                 ",
	"  .0.  0. . .0. 0 .   .   .  0.000.0 . 0.0 . 0.0.00.0   0.0 0. 0 .  0.  0.0 0.0  .0  .  0.0 0.0 0.0 0.0 0.0  .0 0.0  .0  .0  .0 0. 0 . 0.0 0.0  .00 00.00 0.0 0.0 0.0 0 .0 0.0  . 0 .0 0.0 0.0   0.0 0.0 0.  0. 00.000.000.000.000. 0 .000.000.   .   .0 0. 0 .0 0.000.000.000.000.0  .000.000.0 0.0 0.0 0.0 0.0 0.000. 0 .                        ",
	"  .0. 00. . . .000.000.   . 0 .   .0 . 0.0 . 0. .  .0 000.0 0. 0 .000. 00.000.000.000.  0.000.000.000.00 .0  .0 0.00 .00 .0 0.000. 0 . 0.00 .0  .0 0 0.0 00.0 0.000.0 0 .00 .000. 0 .0 0.0 0.0 0 0. 0 .000. 0 .0 0.0 0.0  .0 0.0 0.000.0 0.0 0. 0 . 0 .00 . 0 .000.0 0.0 0.0 0.0 0.000.00 . 0 .0 0.0 0.0 0. 0 .0 0. 00.0  .0   000 0 0 000 000 000 ",
	"  . .   . . .0. 0 .   .   .0  .000.0 . 0.0 . 0. .  .0    .0 0. 0 .0  .  0.  0.  0.0 0.  0.0 0.  0.0 0.0 0.0  .0 0.0  .0  .0 0.0 0. 0 . 0.0 0.0  .0   0.0  0.0 0.0  .0 0 .0 0.  0. 0 .0 0.0 0.0 0 0.0 0. 0 .0  .0 0.0 0.0  .0 0.00 . 0 .  0.0 0. 0 . 0 .0 0. 0 .0 0.0 0.0 0.000.000.0  . 00. 0 .0 0.0 0.000.0 0. 0 .00 . 0 .0   0 0 0 0 00    0 0  0",
	"  .0. 0 .0.0. .   .   .000.0  .   . 0.0 .00.00. .  .00000.000. 0 .000.000.  0.000.000.  0.000.000.0 0.000.000.000.000.0  .000.0 0.000.00.0 0.000.0   0.0  0.000.0  .00 0.0 0.000. 0 .000. 0 . 0 0 .0 0. 0 .000.000.000.000.000.000. 0 .00 .0 0. 0 .00 .0 0.000.0 0.0 0.000.0  .  0.0  .000. 0 .000. 0 .000.0 0. 0 .000.  0.0   0 0 0 0 0    0  0  0",
	"  . .   . .0. .   .   .   .   .   .  .  .  .  . .  .     .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .  .   .   .     .    .   .   .    .   .   .   .   .   .     .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .000 000  0  000 000 0000" 
}
scoreFontAr = {
	".  .0. 000 . . . .   .   .   .  0.   . 0.0 .00.00.  0.0  .0.0 0. 000 . 000 .000  .0000 .0000 .0   0.00000. 0000.00000. 000 . 0000. 000 .0000 . 000 .0000 .00000.00000. 0000.0   0.00000.  000.0   0.0    .0   0.0   0. 000 .0000 . 00  .0000 . 0000.00000.0   0.0   0.0   0.0   0.0   0.00000.     .0    .     .    0.     .  000.     .0    .0.   0.0   .00 .     .     .     .     .     .     .     . 0   .     .     .     .     .     .     .   .0   0 ",
	".  .0.0   0. . .0.   .   .   .  0.000.0 . 0.0 . 0. 0 . 0 .0.0 0.0   0.0   0.  0  .    0.    0.0   0.0    .0    .    0.0   0.0   0.0   0.0   0.0   0.0   0.0    .0    .0    .0   0.  0  .    0.0  0 .0    .00 00.00  0.0   0.0   0.0  0 .0   0.0    .  0  .0   0.0   0.0   0. 0 0 .0   0.    0.0000 .0    . 0000.    0. 000 . 0   . 000 .0    . .    .0  0. 0 .0000 .0000 . 000 .0000 . 000 .0 00 . 0000. 0   .0   0.0   0.0   0.0   0.0   0.00000.  0. 000  ",
	".  .0.    0. . . . 0 .   .   . 0 .   .0 . 0.0 . 0.0  .  0. .   .0 0 0.0   0.  0  . 000 .  00 .0   0.0000 .00000.    0. 000 .0   0.0   0.0000 .0    .0   0.0000 .0000 .0 000.00000.  0  .    0.000  .0    .0 0 0.0 0 0.0   0.0   0.0  0 .0   0. 000 .  0  .0   0.0   0.0 0 0.  0  .0   0.   0 .    0.0000 .0    . 0000.0   0.00000.0   0.0000 .0.   0.0 0 . 0 .0 0 0.0   0.0   0.0   0.0   0.00  0.0    .00000.0   0.0   0.0 0 0.0   0.0   0.    0. 0 .0   0 ",
	".  .0.  00 . . . .000.000.   . 0 .   .0 . 0.0 . 0.0  .  0. .   .0  0 .0   0.  0  .0    .    0. 0000.    0.0   0.   0 .0   0.00000.00000.0   0.0    .0   0.0    .0    .0   0.0   0.  0  .    0.0  0 .0    .0   0.0  00.0   0.0000 .0  0 .0000 .    0.  0  .0   0.0   0.0 0 0. 0 0 . 0 0 .  0  . 0000.0   0.0    .0   0.0000 . 0   . 0000.0   0.0.   0.00  . 0 .0 0 0.0   0.0   0.0   0.0   0.0    . 000 . 0   .0   0.0   0.0 0 0. 000 . 0000.  00 .0  .0   0 ",
	".  . .     . . .0. 0 .   .   .0  .000.0 . 0.0 . 0. 0 . 0 . .   .0    .0   0.  0  .0    .    0.    0.    0.0   0.  0  .0   0.    0.0   0.0   0.0   0.0   0.0    .0    .0   0.0   0.  0  .0   0.0   0.0    .0   0.0   0.0   0.0    .0  0 .0   0.    0.  0  .0   0. 0 0 .0 0 0.0   0.  0  . 0   .0   0.0   0.0    .0   0.0    . 0   .    0.0   0.0.0  0.0 0 . 0 .0 0 0.0   0.0   0.0000 . 0000.0    .    0. 0   .0  00. 0 0 .0 0 0.0   0.    0. 0   . 0 .0   0 ",
	".  .0.  0  .0.0. .   .   .000.0  .   . 0.0 .00.00.  0.0  . .   . 0000. 000 .00000.00000.0000 .    0.0000 .0000 .  0  . 000 .0000 .0   0.0000 . 000 .0000 .00000.0    . 000 .0   0.00000. 000 .0   0.00000.0   0.0   0. 000 .0    . 00 0.0   0.0000 .  0  . 000 .  0  . 0 0 .0   0.  0  .00000. 0000.0000 . 0000. 0000. 0000. 0   .0000 .0   0.0. 00 .0  0.000.0 0 0.0   0. 000 .0    .    0.0    .0000 .  000. 00 0.  0  . 0 0 .0   0.0000 .00000.  0. 000  ",
	".  . .     . .0. .   .   .   .   .   .  .  .  .  .   .   . .   .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     . .    .    .   .     .     .     .     .     .     .     .     .     .     .     .     .     .     .   .      "
}
--[[scoreFontAr = {
	". .0.000. . . .   .   .   .  0.   . 0.0 .00.00.0.00.00000.000. 0 .000.000.0 0.000.000.000.000.000.0000.000 . 000.000 .0000.0000.0000.0  0.000.  0.0  0.0  .0   0.0  0. 00 .000 .000 .0000. 000.000.0  0.0   0.0   0.0   0.0   0.0000.   .0  .   .  0.   . 00.   .0  . 0 . 0 .0  .00 .   .   .   .   .   .   .   . 0 .   .   .   .   .   .   .  0.    0 0                 ",
	"  .0.  0. . .0. 0 .   .   .  0.000.0 . 0.0 . 0.0.00.0   0.0 0. 0 .  0.  0.0 0.0  .0  .  0.0 0.0 0.0  0.0  0.0   .0  0.0   .0   .0   .0  0. 0 .  0.0 0 .0  .00 00.00 0.0  0.0  0.0 0 .0  0.0   . 0 .0  0.0   0.0   0. 0 0 .0   0.   0. 00.0  .000.  0.000. 0 .000.0  .   .   .0 0. 0 .0 0.000.000.000.000.0  .000.000.0 0.0 0.0 0.0 0.0 0.000. 0 .                        ",
	"  .0. 00. . . .000.000.   . 0 .   .0 . 0.0 . 0. .  .0 000.0 0. 0 .000. 00.000.000.000.  0.000.000.0000.000 .0   .0  0.00  .00  .0 00.0000. 0 .  0.00  .0  .0 0 0.0 00.0  0.000 .0 0 .000 .0000. 0 .0  0.0   0.0 0 0.  0  . 000 . 00 .0 0.000.0  .000.0 0.000.0 0.000. 0 . 0 .00 . 0 .000.0 0.0 0.0 0.0 0.000.00 . 0 .0 0.0 0.0 0. 0 .0 0. 00.0  .0   000 0 0 000 000 000 ",
	"  . .   . . .0. 0 .   .   .0  .000.0 . 0.0 . 0. .  .0    .0 0. 0 .0  .  0.  0.  0.0 0.  0.0 0.  0.0  0.0  0.0   .0  0.0   .0   .0  0.0  0. 0 .0 0.0 0 .0  .0   0.0  0.0  0.0   .0 0 .0  0.   0. 0 .0  0. 0 0 .0 0 0. 0 0 .  0  .0   .0 0.0 0.0  .0 0.00 . 0 . 00.0 0. 0 . 0 .0 0. 0 .0 0.0 0.0 0.000.000.0  . 00. 0 .0 0.0 0.000.0 0. 0 .00 . 0 .0   0 0 0 0 00    0 0  0",
	"  .0. 0 .0.0. .   .   .000.0  .   . 0.0 .00.00. .  .00000.000. 0 .000.000.  0.000.000.  0.000.000.0  0.0000. 000.0000.0000.0   .0000.0  0.000.000.0  0.000.0   0.0  0. 00 .0   .00 0.0  0.000 . 0 . 00 .  0  . 0 0 .0   0.  0  .0000.000.000.000.000.000. 0 .000.0 0. 0 .00 .0 0.000.0 0.0 0.000.0  .  0.0  .000. 0 .000. 0 .000.0 0. 0 .000.  0.0   0 0 0 0 0    0  0  0",
	"  . .   . .0. .   .   .   .   .   .  .  .  .  . .  .     .   .   .   .   .   .   .   .   .   .   .    .    .    .    .    .    .    .    .   .   .    .   .     .    .    .    .    .    .    .   .    .     .     .     .     .    .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .000 000  0  000 000 0000"
}]]
scoreFontID = love.image.newImageData(#scoreFontAr[1], 7)
local color = {}
color["."] = {255, 0, 0, 255}
color[" "] = {0, 0, 0, 0}
color["0"] = {255, 255, 255, 255}
for y = 1, 7 do
	local str = scoreFontAr[y]
	for x = 1, #scoreFontAr[1] do
		local char = string.sub(str, x, x)
		scoreFontID:setPixel(x - 1, y - 1, color[char][1], color[char][2], color[char][3], color[char][4])
	end
end

MainFont = love.graphics.newImageFont(scoreFontID, " !?.,:+-_/=()[]<>'\"@0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz<&")
MainFont:setFilter("nearest", "nearest")

GJimage = {
	"----000000000000",
	"----00000000000-",
	"---00-------00--",
	"---00------00---",
	"--00------00----",
	"--00-----000000-",
	"-00------00000--",
	"-00--------00---",
	"00000000--00----",
	"00000000-00-----",
	"------0000------",
	"------000-------",
	"-----000--------",
	"-----00---------",
	"----00----------",
	"----0-----------",
	"---0------------"
}

GJtext = {
	"00000000-00000000-00000000-00000000------0000-00000000-0000-----00000000",
	"00000000-00000000-00000000-00000000------0--0-0------0-0--0-----0------0",
	"00-------00----00-00-00-00-00------------0--0-0------0-0--0-----0------0",
	"00-00000-00000000-00-00-00-00000000------0--0-0--00--0-0--0-----000--000",
	"00-00000-00000000-00-00-00-00000000--00000--0-0--00--0-0--00000---0--0--",
	"00----00-00----00-00-00-00-00--------0------0-0------0-0------0---0--0--",
	"00000000-00----00-00-00-00-00000000--0------0-0------0-0------0---0--0--",
	"00000000-00----00-00-00-00-00000000--00000000-00000000-00000000---0000--"
}

IntroImage = {
	"----------0-----",
	"----------00----",
	"----------0000--",
	"---------0000-0-",
	"---000---0000000",
	"--00-----0000000",
	"-00-------00000-",
	"-00-------0000--",
	"00-------00000--",
	"00-----0000000--",
	"00----00000000--",
	"00---00000000---",
	"00---00000000---",
	"00--0000000000--",
	"00--0000000000--",
	"-0000000000000--",
	"--0000000000-00-"
}

--[[IntroImage = {
	"----------------------00000--------------",
	"------------------0000000----------------",
	"----------------00000000-----------------",
	"--------------000000000----00000---------",
	"------------0000000000000000000----------",
	"----------00000000000000000000-----------",
	"--------0000000000000000000000-----------",
	"-------000000000000000000----000---------",
	"------00000000000000000000----0000-------",
	"------000000000000000000000000000000-----",
	"-----000000000000000000000000000000000---",
	"----00000000000000000000000000000000000--",
	"----00000000000000000000000000000000000--",
	"----0000000000000000000000000000000000---",
	"---000000000000000000000000000000000-----",
	"---000000000000000000000000000000--------",
	"--00000000000000000000000000-------------",
	"--0000000000000000000000-----------------",
	"--000000000000000000000------------------",
	"-000000000000000000000-------------------",
	"-000000000000000000000-------------------",
	"-0000000000000000000000------------------",
	"-00000000000000000000000-----------------",
	"000000000000000000000000-----------------",
	"0000000000000000000000000----------------",
	"00000000000000000000000000---------------",
	"000000000000000000000000000--------------",
	"0000000000000000000000000000-------------",
	"--000000000000000000000000000------------",
	"-----0000000000000000000000000-----------",
	"----------00000000000000000000-----------"
}]]

LoveImage = {
	"---0000---",
	"--000000--",
	"-00000000-",
	"00-0000-00",
	"0-0-00-0-0",
	"0000000000",
	"0000000000",
	"-00000000-",
	"--000000--",
	"---0000---"
}

Arrow = {
	"0--",
	"00-",
	"000",
	"00-",
	"0--"
}

PointerMain = {
	"-00-",
	"0--0",
	"0--0",
	"-00-"
}
PointerSub = {
	"----",
	"-00-",
	"-00-",
	"----"
}

function CorrectName(n)
	local newName = ""
	for i = 1, #n do
		local char = string.sub(n, i, i)
		if char == " " then
			char = "%20"
		end
		if char == "\n" then
			char = "%20||%20"
		end
		if char == "\t" then
			char = "%20"
		end
		newName = newName .. char
	end
	return newName
end

GJimage = createImage(GJimage)
GJtext = createImage(GJtext)

LoveImage = createImage(LoveImage)

IntroImage = createImage(IntroImage, 16, 17)

Arrow = createImage(Arrow)

PointerMain = createImage(PointerMain)
PointerSub = createImage(PointerSub)

IntroText = "VladTheCat"
IntroTextW = MainFont:getWidth(IntroText) * 4

NameText = "One Bit Arena"
NameTextW = MainFont:getWidth(NameText) * 4

stdf = gr.newFont(10)
gr.setFont(MainFont)


screen = gr.newCanvas(1024, 1024)
screen:setWrap("repeat", "repeat")

FIRSTPLAY = false
FIRSTQUESTION = true
local hasFile = love.filesystem.exists("NotFirstPlay")
if not hasFile then
	FIRSTPLAY = true
	local file = love.filesystem.newFile("NotFirstPlay")
	file:open("w")
	file:write("Don't remove that file, if you don't want play tutorial every time")
	file:flush()
	file:close()
end
function ShowTutor(show)
	ShowedTutor = {
		move = not show,
		shoot = not show,
		kill = not show,
		stuck = not show,
		skill = not show,
		frisbee1 = not show,
		frisbee2 = not show,
		pickup = not show,
		health = not show,
		newEnemy = not show,
		highscores = not show,
		endScreen = not show
	}
end

ShowTutor(true)
if not FIRSTPLAY then
	ShowTutor(false)
end
TutorText = {
	move = "Use W/S/A/D for move \nand mouse for look around",
	shoot = "There is an enemy!\nUse LMB to shoot them",
	stuck = "Oops! Your disk has stuck.\nYou can pick it up\nor use right mouse button\nto get it back.",
	skill = "Now your skillbar is empty.\nWait until it is filled.\nOr you can find skill buff (marked with 'S')",
	frisbee1 = "Frisbee have another skill-action.\nIf you use skill (use right mouse button)\nwith disk in hand, disk will\nshot enemies throught.",
	frisbee2 = "This disk have skill-attack.\nIf you use skill (use right mouse button)\nwith disk in hand, disk will\nshot enemies throught.",
	kill = "Your first kill...\nEvery kill gives you some score.\nIf you kill a few enemies in one throw\nyou will get more score\nand your combo counter will increase by 1.\nMore combo - More score!",
	newEnemy = "Now you will see new enemy.\nIf you get some score, then\nthere will be new types of enemies.\nIf you get 20000 points,\nyou will see a Boss.",
	endScreen = "Here you can submit your score to online table.\nFinal score calculate from:\nfinal score = score x kills / time.",
	highscores = "Here you can see highscores for your type of disk.\nAlso you can copy link to some highscore\nfor share it with friends.",
	pickup = "You have picked up the buff.\nIt can increase your abilities\nor make you invulnerable.",
	health = "You have picked up the health-box.\nIt fills your health"
}

function DebugPlaceEnemies()
	SpawnEnemies = false
	PlayerImmortal = true
	EnemyAI = false

	for i = 1, 300 do
		Enemy.place(1, math.random(50, mapW * mapS - 50), math.random(50, mapH * mapS - 50))
	end
end


--#####################################################################################################################################
--#####################################################################################################################################

function love.load(arg)

	SpawnEnemies = true
	SpawnPickups = true
	PlayerImmortal = false
	EnemyAI = true

	SPLASHES = {}
	
	gr.setBackgroundColor(196, 207, 161)

	m_text = {"START", string.upper(DiskStates[1]), "RECREATE", "STOP", "HIGHSCORES", "SETTINGS", "CREDITS", "LOGIN", "EXIT"}
	s_text = {"SOUND: ON", "MUSIC: ON", "FULLSCREEN", "BACK"}
	mapWidth = 55
	mapHeight = 55
	gr.setColor(255, 255, 255)
	screen:clear()
	state = State.intro

	State.game.load()
	State.intro.load()

	Score = 0
	Time = 0

	EndScore = 0
	started = false
	EndCanvas = gr.newCanvas(1024, 1024)
	GAME_CANVAS = screen

	PLAYMUSIC = true
	DeltaTime_All = 0
	averageDT = 0
	averageDTCount = 0


	local hasFile = love.filesystem.exists("settings")
	if not hasFile then
		local file = love.filesystem.newFile("settings")
		file:open("w")
		file:write("110", 3)
		file:flush()
		file:close()
	else
		local file = love.filesystem.newFile("settings")
		file:open("r")
		local data = file:read(3)
		file:close()
		local set = string.sub(data, 1, 1)
		print(set)
		if set == "0" then
			love.audio.setVolume(0)
		else
			love.audio.setVolume(1)
		end
		set = string.sub(data, 2, 2)
		print(set)
		if set == "0" then
			PLAYMUSIC = false
		else
			PLAYMUSIC = true
		end
		set = string.sub(data, 3, 3)
		print(set)
		if set == "0" then
			love.window.setFullscreen(false)
		else
			love.window.setFullscreen(true)
		end
	end
end

function love.update(DELTA)
	if not love.window.getFullscreen() and love.window.getWidth() ~= 800 then
		love.window.setMode(800, 600)
		tilemap.redraw()
	end
	UPDATE_GAME = true
	SHOWED_SPLASH = false

	delta = DELTA

	evilib.debugUpdate()

	activitiesRemove = {}
	ScoretablesRemove = {}
	dt = delta

	mx, my = love.mouse.getPosition()
	if dt < 0.2 and evilib.activeDebug ~= evilib.console then
		state.update(dt)
		if state == State.game then
			love.mouse.setGrabbed(true)
		else
			love.mouse.setGrabbed(false)
		end
	end
	add(string.format("Mouse: (%d, %d)", mx, my))

	keysPressed = { }
	keysReleased = { }
	buttonsPressed = { }
	buttonsReleased = { }
end


function love.draw()
	gr.setFont(MainFont)
	gr.setColor(255, 255, 255)

	screen:clear()
	gr.push()
	gr.setCanvas(screen)
		state.draw()
		--[[
		gr.print("HEALTH", 8, 8, 0, 4, 4)
		gr.print("SKILL", 114, 8, 0, 4, 4)

		gr.print("KILLS "..StringZero(Kills, 6), 8, 526, 0, 4, 4)
		gr.print("TIME  "..StringZero(math.floor(Time), 6), 8, 550, 0, 4, 4)
		gr.print("SCORE "..StringZero(Score, 6), 8, 574, 0, 4, 4)
		]]
	gr.setCanvas()
	gr.pop()
	gr.setCanvas(EndCanvas)
		EndCanvas:clear()
		gr.draw(screen)
		DrawPointer()
	gr.setCanvas()
	GAME_CANVAS = screen
	-- tic()
	-- gr.setShader(myShader)
	gr.setColor(35, 35, 35, 255)
	gr.draw(EndCanvas, 0, 0)
	gr.setColor(35, 35, 35, 120)
	gr.draw(EndCanvas, 2, 2)

	-- gr.setShader()
	-- DeltaTime_All = DeltaTime_All + toc()
	-- averageDTCount = averageDTCount + 1
	-- add(DeltaTime_All / averageDTCount)
	gr.setColor(255, 255, 255)
	gr.push()
	gr.translate(math.floor(-player.x + 400 + camDX), math.floor(-player.y + 300 + camDY))
	Quad.draw()
	gr.pop()

	evilib.drawDebug()
end

function DrawPointer()
	local mx, my = love.mouse.getPosition()
	gr.draw(PointerMain, mx, my, 0, 4, 4, 2, 2)
	gr.setBlendMode("subtractive")
	gr.draw(PointerSub, mx, my, 0, 4, 4, 2, 2)
	gr.setBlendMode("alpha")
end

function love.quit()
	if GJ.isLoggedIn then
		GJ.closeSession()
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

function scoreAdd(s, x, y)
	Score = Score + s * DIFFICULT
	Score1 = Score1 - s * DIFFICULT
	Score2 = tostring(tonumber(Score2) - s * DIFFICULT)
	local sc = {}
	sc.text = tostring("+"..s * DIFFICULT)
	sc.textw = MainFont:getWidth(sc.text)
	sc.texth = MainFont:getHeight(sc.text)
	sc.x = x or player.x
	sc.y = y or player.y
	sc.time = 2
	table.insert(Scoretables, sc)
end

function love.focus(f)
	if not f then
		if state == State.game then
			Music[currentTrack]:pause()
			state = State.pause
			state.load(true)
			love.mouse.setGrabbed(false)
		end
	end
end

SCREENSHOT = 0
function love.keypressed(key, unicode)
	keysPressed[key] = true
end

function love.keyreleased(key)
	keysReleased[key] = true
end

function love.mousepressed(x, y, b)
	buttonsPressed[b] = true
end

function love.mousereleased(x, y, b)
	buttonsReleased[b] = true
end

function keyPressed(key)
	if (keysPressed[key]) then
		return true
	else 
		return false
	end
end

function keyReleased(key)
	if (keysReleased[key]) then
		return true
	else
		return false
	end
end

function mousePressed(key)
	if (buttonsPressed[key]) then
		return true
	else
		return false
	end
end

function mouseReleased(key)
	if (buttonsReleased[key]) then
		return true
	else
		return false
	end
end

function love.textinput(t)
	if evilib.activeDebug ~= evilib.console then
		if state == State.endScreen and not GJ.isLoggedIn then
			local byte = string.byte(t)
			if ((byte >= 65 and byte <= 90) or (byte >= 48 and byte <= 57) or (byte >= 97 and byte <= 122) or byte == 32 or byte == 45 or byte == 95) and #Name < 15 then
				local place = t
				if t == " " and #Name == 0 then
					place = ""
				end
				Name = Name .. place
				SndClick:play()
			end
		end
		if state == State.login and State.login.input == true then
			local st = State.login
			local byte = string.byte(t)
			if ((byte >= 65 and byte <= 90) or (byte >= 48 and byte <= 57) or (byte >= 97 and byte <= 122) or byte == 32 or ((byte == 45 or byte == 95) and st.activeInput == "name")) and #st.fields[st.activeInput] < 25 then
				local place = t
				if t == " " and #st.fields[st.activeInput] == 0 then
					place = ""
				end
				st.fields[st.activeInput] = st.fields[st.activeInput] .. place
				SndClick:play()
			end
		end
	else
		local byte = string.byte(t)
		if byte < 128 then
			evilib.keystring = evilib.keystring .. t
			evilib.console.textInput(t)
		end
	end
end

function inBounds(n, min, max)
	return n >= min and n <= max
end

function inBounds2(x, y, xmin, ymin, xmax, ymax)
	return x >= xmin and x <= xmax and y >= ymin and y <= ymax
end

function hasActor(tab, act)
	local has = false
	table.foreachi(tab, function(i, v)
		if v == act then 
			has = true
			return
		end
	end)
	return has
end

function DrawGUIRect()
	drawRectangle("line", 0, 0, 200, 600)
end

function StringZero(s, l)
	local str = tostring(s)
	for i = 1, l - #str do
		str = "0" .. str
	end
	return str
end

function choose( ... )
	local a = {...}
	return a[math.random(#a)]
end

function tic()
	_timer = love.timer.getTime()
end

function toc()
	return love.timer.getTime() - _timer
end

function cycle(value, mn, mx)
	local res, d
	d = (mx - mn)
	res = (value - mn) % d
	if res < 0 then
		res = res + d
	end
	return mn + res
end

function angleDiff(ang1, ang2)
	return cycle(ang2 - ang1, -math.pi, math.pi)
end

function slowDir(ang1, ang2, spd)
	local dt = love.timer.getDelta()
	local diff
	diff = cycle(ang2 - ang1, -math.pi, math.pi)
	-- clamp rotations by speed:
	if diff < -spd then return ang1 - spd end
	if diff > spd then return ang1 + spd end
	-- if difference within speed, rotation's done:
	return ang2
end

function sign(n)
	if n ~= 0 then
		return n / math.abs(n)
	end
	return 0
end

function ActorMove(act, hs, vs, d)
	local dt = dt
	if not d then
		dt = 1
	end
	local DIST = act.radius
	local ty, by = act.y - DIST, act.y + DIST
	local hsdt = hs * dt
	local hss = sign(hsdt)
	local x, ty, tt = tilemap.get(act.x + DIST * hss + hsdt, ty, true)
	local x, cy, ct = tilemap.get(act.x + DIST * hss + hsdt, act.y, true)
	local x, by, bt = tilemap.get(act.x + DIST * hss + hsdt, by, true)
	act.x = act.x + hsdt
	if act.solid then
		if hsdt > 0 then
			if tt == 2 or tt == 3 or bt == 2 or bt == 3 or ct == 2 or ct == 3 then
				act.x = x * mapS - DIST - 01
				act.hs = 0
			end
		elseif hsdt < 0 then
			if tt == 2 or tt == 3 or bt == 2 or bt == 3 or ct == 2 or ct == 3 then
				act.x = x * mapS + DIST + mapS
				act.hs = 0
			end
		end
	end
		local lx, rx = act.x - DIST, act.x + DIST
		local vsdt = vs * dt
		local vss = sign(vsdt)
		local lx, y, lt = tilemap.get(lx, act.y + DIST * vss + vsdt, true)
		local cx, y, ct = tilemap.get(act.x, act.y + DIST * vss + vsdt, true)
		local rx, y, rt = tilemap.get(rx, act.y + DIST * vss + vsdt, true)

	act.y = act.y + vsdt
	if act.solid then
		if vsdt > 0 then
			if lt == 2 or lt == 3 or rt == 2 or rt == 3 or ct == 2 or ct == 3 then
				act.y = y * mapS - DIST - 01
			end
		elseif vsdt < 0 then
			if lt == 2 or lt == 3 or rt == 2 or rt == 3 or ct == 2 or ct == 3 then
				act.y = y * mapS + DIST + mapS
			end
		end
	end
end

function round(n, m)
	return math.floor(n * 10^m) / 10^m
end

function drawRectangle(mode, x, y, w, h)
	_rect[mode](x, y, w, h)
end

_rect = {}
_rect["fill"] = function(x, y, w, h)
	gr.rectangle("fill", x, y, w, h)
end

_rect["line"] = function(x, y, w, h)
	gr.rectangle("fill", x, y, w, h)
	gr.setBlendMode("subtractive")
	gr.rectangle("fill", x + 4, y + 4, w - 8, h - 8)
	gr.setBlendMode("alpha")
end

function rnd(n)
	return math.floor(n + 0.5)
end 

function printC(t, x, y, size)
	gr.setFont(MainFont)
	local w, h = MainFont:getWidth(t) * size, MainFont:getHeight(t) * size
	local ncount = char_count(t, "\n")
	local xx, yy = x - w / 2 + size / 2, y - h / 2 - h * ncount / 2
	gr.print(t, xx, yy, 0, size, size)
end

function div(n, m)
	return math.floor(n / m)
end

function showSplash(text, x, y, xa, ya)
	local t = text
	local EndCanvas = gr.newCanvas(1024, 1024)
	local splash = gr.newCanvas(1024, 1024)
	local xsize, ysize = 2, 2
	local width, height = 0, 0
	width = MainFont:getWidth(t) * 3 + 16
	height = MainFont:getHeight(t) * 3
	height = height + height * char_count(t, "\n") + 16
	gr.setColor(255, 255, 255, 255)
	gr.setCanvas(splash)
	gr.draw(screen)
	gr.setCanvas()
	local xof, yof = {["left"] = 0, ["center"] = 0.5, ["right"] = 1}, {["top"] = 0, ["center"] = 0.5, ["bottom"] = 1}
	xoffset = xof[xa]
	yoffset = yof[ya]
	while xsize < width or ysize < height do
		xsize = math.min(xsize + 8, width)
		ysize = math.min(ysize + 8, height)
		gr.setCanvas(splash)
		gr.setColor(255, 255, 255)
		drawRectangle("line", x - xsize * xoffset, y - ysize * yoffset, xsize, ysize)
		if xsize >= width and ysize >= height then
			printC(t, x - (xsize * xoffset) + xsize / 2, y - (ysize * yoffset) + ysize / 2, 3)
		elseif xsize < width or ysize < height then
			--DrawPointer()
		end
		gr.setCanvas()
		gr.clear()
		gr.setColor(35, 35, 35)
		gr.draw(splash)
		gr.setColor(35, 35, 35, 120)
		gr.draw(splash, 2, 2)
		gr.present()
	end
	gr.clear()
	while true do
		love.event.pump()

		for e, a, b, c in love.event.poll() do
			if (e == "keypressed" and (a == "return" or a == "escape" or a == " ")) or (e == "mousepressed" and c == "l") then
				return
			end
		end
		gr.setCanvas(EndCanvas)
		EndCanvas:clear()
		gr.setColor(255, 255, 255)
		gr.draw(splash)
		DrawPointer()
		gr.setCanvas()
		gr.clear()
		gr.setColor(35, 35, 35, 255)
		gr.draw(EndCanvas)
		gr.setColor(35, 35, 35, 120)
		gr.draw(EndCanvas, 2, 2)
		gr.present()

		if love.timer then
			love.timer.sleep(0.01)
		end
	end
	UPDATE_DELTA = 0
end

function showSplashQuestion(text, x, y, xa, ya)
	local t = text .. "\n[Y/N]"
	local EndCanvas = gr.newCanvas(1024, 1024)
	local splash = gr.newCanvas(1024, 1024)
	local xsize, ysize = 2, 2
	local width, height = 0, 0
	width = MainFont:getWidth(t) * 3 + 16
	height = MainFont:getHeight(t) * 3
	height = height + height * char_count(t, "\n") + 16
	gr.setColor(255, 255, 255, 255)
	gr.setCanvas(splash)
	gr.draw(screen)
	gr.setCanvas()
	local xof, yof = {["left"] = 0, ["center"] = 0.5, ["right"] = 1}, {["top"] = 0, ["center"] = 0.5, ["bottom"] = 1}
	xoffset = xof[xa]
	yoffset = yof[ya]
	while xsize < width or ysize < height do
		xsize = math.min(xsize + 8, width)
		ysize = math.min(ysize + 8, height)
		gr.setCanvas(splash)
		gr.setColor(255, 255, 255)
		drawRectangle("line", x - xsize * xoffset, y - ysize * yoffset, xsize, ysize)
		if xsize >= width and ysize >= height then
			printC(t, x - (xsize * xoffset) + xsize / 2, y - (ysize * yoffset) + ysize / 2, 3)
		elseif xsize < width or ysize < height then
			--DrawPointer()
		end
		gr.setCanvas()
		gr.clear()
		gr.setColor(35, 35, 35)
		gr.draw(splash)
		gr.setColor(35, 35, 35, 120)
		gr.draw(splash, 2, 2)
		gr.present()
	end
	gr.clear()
	while true do
		love.event.pump()
		for e, a, b, c in love.event.poll() do
			if e == "keypressed" then --[[or (e == "mousepressed" and c == "l") then]]
				if a == "y" or a == "return" then
					return true
				elseif a == "n" or a == "escape" then
					return false
				end
			end
		end
		gr.setCanvas(EndCanvas)
		EndCanvas:clear()
		gr.setColor(255, 255, 255)
		gr.draw(splash)
		DrawPointer()
		gr.setCanvas()
		gr.clear()
		gr.setColor(35, 35, 35, 255)
		gr.draw(EndCanvas)
		gr.setColor(35, 35, 35, 120)
		gr.draw(EndCanvas, 2, 2)
		gr.present()

		if love.timer then
			love.timer.sleep(0.01)
		end
	end
	UPDATE_DELTA = 0
end

function AddSplash(text, x, y, xa, ya)
	table.insert(SPLASHES, 1, {text = text, t = "s", x = x, y = y, xa = xa, ya = ya})
end

function AddSplashQ(text, x, y, xa, ya)
	table.insert(SPLASHES, 1, {text = text, t = "q", x = x, y = y, xa = xa, ya = ya})
end

function char_count(str, char) 
	if not str then
		return 0
	end

	local count = 0 
	local byte_char = string.byte(char)
	for i = 1, #str do
		if string.byte(str, i) == byte_char then
			count = count + 1 
		end 
	end 
	return count
end

function ShowTutorText(text)
	showSplash(text, 400, 350, "center", "top")
end

function openLink(url)
	-- local _os = love.system.getOS()
	-- if _os == "OS X" then
	-- 	os.execute([[open ]]..url)
	-- elseif _os == "Linux" then
	-- 	os.execute([[xdg-open "]]..url..[[" ]])
	-- else
	-- 	os.execute([[start ]]..url)
	-- end
	love.system.openURL(url)
end

function SecondsToTime(s)
	return StringZero(math.floor(s/60), 2)..":"..StringZero(math.floor(s%60), 2)
end