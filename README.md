# One Bit Arena #

* Author: [VladTheCat](http://vk.com/dev.vladthecat)
* Operation System: Windows XP, Vista, 7, 8; Linux (in future, maybe...)
* Engine: Love2D v0.9.1
* Programming language: Lua

## How to build and launch ##

* Download Love2D from [here](http://love2d.org/) and install
* Download sources or clone repository
* Pack it to .zip archive and rename to .love
* Launch it
* ....
* PROFIT!

## Controls ##

WASD - Move player 
LMB - Throw disk 
RMB - Use skill 

---------

So, that's all. GL, guys :)