disk = {
	throwed = false,
	x = 0,
	y = 0,
	dir = 0,
	move = false,
	time = 0,
	angle = 0,
	permanentHome = false
}

disk.img = {
	["disk"] = createImage({
		"--00--",
		"-0--0-",
		"0----0",
		"0----0",
		"-0--0-",
		"--00--"
	}),
	["boomerang"] = createImage({
		"--000-",
		"-000--",
		"000---",
		"000---",
		"-000--",
		"--000-"
	}),
	["chakram"] = createImage({
		"--00--",
		"-00-0-",
		"00----",
		"00----",
		"-00-0-",
		"--00--"
	}),
	["frisbee"] = createImage({
		"--00--",
		"-0--0-",
		"0-00-0",
		"0-00-0",
		"-0--0-",
		"--00--"
	})
}

function disk.init(Act)
	disk.throwed = false
	disk.x = 0
	disk.y = 0
	disk.dir = 0
	disk.move = false
	disk.combo = 0
	disk.comboTime = 0
	disk.strike = 0
	disk.time = 0
	disk.angle = 0
	disk.permanentHome = false
	disk.action = Act
end

function disk.throw(x, y, dir)
	if disk.throwed == true then return end
	disk.throwed = true
	disk.x = x
	disk.y = y
	disk.dir = dir
	disk.move = true
	disk.load[disk.action]()
	disk.angle = dir
	disk.strike = 0
	disk.speed = 500
	if Kills / Time > 1 then
		disk.speed = 800
	end
	disk.canCatch = false
	disk.skilled = false
	disk.miss = true
	SndThrow:play()
end

function disk.TutorStuck()
	if not ShowedTutor.stuck then
		ShowTutorText(TutorText.stuck)
		ShowedTutor.stuck = true
	end
end

function disk.update(dt)
	if disk.throwed and (disk.move or disk.permanentHome) then
		if not disk.permanentHome then
			disk.moveAction[disk.action](dt)
		else
			disk.dir = math.atan2(player.y - disk.y, player.x - disk.x)
			disk.x = disk.x + math.cos(disk.dir) * 500 * dt
			disk.y = disk.y + math.sin(disk.dir) * 500 * dt
		end
		disk.angle = disk.angle + pi * 5 * dt
	elseif not disk.throwed and not player.dead then
		disk.x = player.x + 20 * cos(player.angle + pi / 2)
		disk.y = player.y + 20 * sin(player.angle + pi / 2)
		disk.angle = player.angle
	end
	if not disk.move and disk.action ~= "frisbee" then
		disk.combo = 0
		disk.comboTime = 0
		disk.strike = 0
	end
	if disk.throwed and disk.canCatch then
		if math.sqrt((disk.x - player.x)^2 + (disk.y - player.y)^2) < 25 then
			disk.throwed = false
			disk.move = false
			disk.permanentHome = false
			disk.canCatch = false
			if disk.miss then
				disk.combo = 0
			end
		end
	end
end

disk.moveAction = {}
disk.hitAction = {}
disk.load = {}
disk.motion = {}

disk.load["disk"] = function()
	disk.time = 0.3
	disk.flying = 0
	disk.touches = 0
end

disk.load["boomerang"] = function()
	disk.time = 0.5
	disk.flying = 0
	disk.touches = 0
end

disk.load["chakram"] = function()
	disk.flying = 0
	disk.touches = 0
	disk.speed = 700
end

disk.load["linear"] = function()
	disk.distance = 0
	disk.flyState = 0
end

disk.load["frisbee"] = function()
	disk.distance = 0
end

disk.moveAction["disk"] = function(dt)
	disk.time = disk.time - dt
	disk.flying = disk.flying + dt
	if disk.time <= 0 and disk.speed > 400 then
		disk.dir = slowDir(disk.dir, math.atan2(player.y - disk.y, player.x - disk.x), pi * 3 * dt)
		disk.canCatch = true
	end
	if disk.flying > 5 then
		disk.speed = math.max(0, disk.speed - 250 * dt)
		if disk.speed == 0 then
			disk.move = false
			disk.TutorStuck()
		end
	end

	for i = 0, disk.speed, 4 do
		disk.xprev = disk.x
		disk.yprev = disk.y
		disk.x = disk.x + 4 * cos(disk.dir) * dt
		disk.y = disk.y + 4 * sin(disk.dir) * dt
		local x, y, t = tilemap.get(disk.x, disk.y, true)
		if (t == 2 or t == 3) and not disk.permanentHome then
			disk.move = false
			disk.time = 0
			disk.canCatch = true
			SndStuck:play()
			disk.TutorStuck()
			break
		end
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

disk.moveAction["boomerang"] = function(dt)
	disk.time = disk.time - dt
	disk.flying = disk.flying + dt
	if disk.time <= 0 and disk.speed > 400 then
		disk.dir = slowDir(disk.dir, math.atan2(player.y - disk.y, player.x - disk.x), pi * 1.5 * dt)
		disk.canCatch = true
	end
	if disk.flying > 5 then
		disk.speed = math.max(0, disk.speed - 250 * dt)
		if disk.speed == 0 then
			disk.move = false
			disk.TutorStuck()
		end
	end

	local hs, vs = disk.speed * math.cos(disk.dir), disk.speed * math.sin(disk.dir)
	local x, y, t = tilemap.get(disk.x + hs * dt, disk.y, true)
	if (t == 2 or t == 3) and not disk.permanentHome then
		hs = -hs
		disk.canCatch = true
		disk.touches = disk.touches + 1
		SndStuck:play()
		disk.TutorStuck()
	end
	x, y, t = tilemap.get(disk.x, disk.y + vs * dt, true)
	if (t == 2 or t == 3) and not disk.permanentHome then
		vs = -vs
		disk.canCatch = true
		disk.touches = disk.touches + 1
		SndStuck:play()
		disk.TutorStuck()
	end
	disk.dir = math.atan2(vs, hs)
	disk.x = disk.x + math.cos(disk.dir) * disk.speed * dt
	disk.y = disk.y + math.sin(disk.dir) * disk.speed * dt
	if disk.touches > 10 then
		disk.canCatch = true
		disk.permanentHome = true
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

disk.moveAction["chakram"] = function(dt)
	disk.flying = disk.flying + dt
	if disk.flying > 5 then
		disk.canCatch = true
		disk.speed = math.max(0, disk.speed - 250 * dt)
		if disk.speed == 0 then
			disk.move = false
			disk.TutorStuck()
		end
	end

	local hs, vs = disk.speed * math.cos(disk.dir), disk.speed * math.sin(disk.dir)
	local x, y, t = tilemap.get(disk.x + hs * dt, disk.y, true)
	if (t == 2 or t == 3) and not disk.permanentHome then
		hs = -hs
		disk.canCatch = true
		disk.touches = disk.touches + 1
		SndStuck:play()
	end
	x, y, t = tilemap.get(disk.x, disk.y + vs * dt, true)
	if (t == 2 or t == 3) and not disk.permanentHome then
		vs = -vs
		disk.canCatch = true
		disk.touches = disk.touches + 1
		SndStuck:play()
	end
	disk.dir = math.atan2(vs, hs)
	disk.x = disk.x + math.cos(disk.dir) * disk.speed * dt
	disk.y = disk.y + math.sin(disk.dir) * disk.speed * dt
	if disk.touches > 5 then
		disk.canCatch = true
		disk.permanentHome = true
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

disk.moveAction["linear"] = function(dt)
	if disk.flyState == 0 and disk.distance > 200 then
		disk.flyState = 1
		disk.canCatch = true
		disk.dir = disk.dir + pi
	end
	if disk.flyState == 1 and disk.distance < -100 then
		disk.speed = math.max(0, disk.speed - 1000 * dt)
		if disk.speed == 0 then
			disk.move = false
		end
	end

	local mult = 1
	if disk.flyState == 1 then
		mult = -1
	end
	disk.distance = disk.distance + disk.speed * mult * dt

	for i = 0, disk.speed, 4 do
		disk.xprev = disk.x
		disk.yprev = disk.y
		disk.x = disk.x + 4 * cos(disk.dir) * dt
		disk.y = disk.y + 4 * sin(disk.dir) * dt
		local x, y, t = tilemap.get(disk.x, disk.y, true)
		if (t == 2 or t == 3) and not disk.permanentHome then
			disk.move = false
			-- disk.dir = disk.dir + pi
			-- disk.flyState = 1
			disk.canCatch = true
			SndStuck:play()
			break
		end
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

disk.moveAction["frisbee"] = function(dt)
	disk.distance = disk.distance + disk.speed * dt
	if disk.distance > 400 then
		disk.permanentHome = true
		disk.canCatch = true
		return
	end
	disk.x = disk.x + disk.speed * cos(disk.dir) * dt
	disk.y = disk.y + disk.speed * sin(disk.dir) * dt
	local x, y, t = tilemap.get(disk.x, disk.y, true)
	if (t == 2 or t == 3) and not disk.permanentHome then
		disk.permanentHome = true
		disk.canCatch = true
		SndStuck:play()
		return
	end
end

disk.hitAction["frisbee"] = function()
	if not disk.skilled then
		disk.permanentHome = true
		disk.canCatch = true
	end
end