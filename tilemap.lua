tilemap = {}
tileImage = {}

tiles = {
	{
		"--------",
		"-------0",
		"--0-----",
		"--------",
		"-----0--",
		"--------",
		"-0------",
		"-----0--"
	},
	{
		"00000000",
		"0-0000-0",
		"00000000",
		"000--000",
		"000--000",
		"00000000",
		"0-0000-0",
		"00000000"
	},
	{
		"00000000",
		"0------0",
		"0-0000-0",
		"0-0--0-0",
		"0-0--0-0",
		"0-0000-0",
		"0------0",
		"00000000"
	},
	{
		"--------",
		"--------",
		"--------",
		"--------",
		"--------",
		"--------",
		"--------",
		"--------"
	},
	{
		"00--00--",
		"--00--00",
		"--------",
		"--------",
		"00--00--",
		"--00--00",	
		"--------",
		"--------"
	},
	{
		"0------0",
		"0------0",
		"0------0",
		"0-0000-0",
		"0------0",
		"0------0",
		"0------0",
		"0-0000-0"
	},
	{
		"00000000",
		"--------",
		"0---0---",
		"0---0---",
		"0---0---",
		"0---0---",
		"--------",
		"00000000"
	}
}

for i, v in ipairs(tiles) do
	tiles[i] = createImage(tiles[i])
end



function tilemap.createNew(tilesX, tilesY)
	gr.setColor(255, 255, 255)
	if not map then
		map = {}
	end
	mapS = 32
	sizeK = mapS / 8
	mapW = tilesX - 1
	mapH = tilesY - 1
	if not mapC then
		mapC = {}
	end
	for x = 0, tilesX * mapS / 1024 do
		if not mapC[x] then
			mapC[x] = {}
		end
		for y = 0, tilesY * mapS / 1024 do
			if not mapC[x][y] then
				mapC[x][y] = gr.newCanvas(1024, 1024)
			elseif type(mapC[x][y]) == "canvas" then
				mapC[x][y].clear()
			end
		end
	end
	for x = 0, tilesX - 1 do
		if not map[x] then
			map[x] = {}
		end
		for y = 0, tilesY - 1 do
			map[x][y] = 0
		end
	end
end

function tilemap.generateSymmetric()
	gr.setColor(255, 255, 255)
	local template = {}
	for x = 0, mapW / 2 do
		template[x] = {}
		for y = 0, mapH / 2 do
			template[x][y] = choose(0, 0, 1)
			--if x % 2 == 1 and y % 2 == 1 then
				if math.random(25) < 3 then
					template[x][y] = choose(2, 3)
				end
			--end
			if x == 0 or y == 0 then
				template[x][y] = 2
			end
			if x > mapW / 2 - 2 and y > mapH / 2 - 2 then
				template[x][y] = 0
			end
			local t = template
			tilemap.place(t[x][y], x, y)
			tilemap.place(t[x][y], mapW - x, y)
			tilemap.place(t[x][y], mapW - x, mapH - y)
			tilemap.place(t[x][y], x, mapH - y)
		end
	end
end

function tilemap.generateEmpty()
	gr.setColor(255, 255, 255)
	local template = {}
	for x = 0, mapW / 2 do
		template[x] = {}
		for y = 0, mapH / 2 do
			template[x][y] = choose(0, 0, 1)
			if x == 0 or y == 0 then
				template[x][y] = 2
			end
			if x > mapW / 2 - 2 and y > mapH / 2 - 2 then
				template[x][y] = 0
			end
			local t = template
			tilemap.place(t[x][y], x, y)
			tilemap.place(t[x][y], mapW - x, y)
			tilemap.place(t[x][y], mapW - x, mapH - y)
			tilemap.place(t[x][y], x, mapH - y)
		end
	end
end

function tilemap.createFromImg(img)
	local id = love.image.newImageData(img)
	local iw, ih = id:getWidth(), id:getHeight()
	for x = 0, iw - 1 do
		for y = 0, ih - 1 do
			local r, g, b, a = id:getPixel(x, y)
			if a > 0 then
				local b = 0
				if r == 0 and g == 0 and b == 0 then
					b = 2
				elseif r == 50 and g == 50 and b == 50 then
					b = 3
				elseif r == 0 and g == 128 and b == 255 then
					b = 4
				end
				tilemap.place(b, x, y)
			end
		end
	end
end

function tilemap.place(t, px, py, r)
	local x, y = px, py
	if r then
		x = math.floor(x / mapS)
		y = math.floor(y / mapS)
	end
	if x >= 0 and y >= 0 and x <= mapW and y <= mapH then
		map[x][y] = t
		gr.setCanvas(mapC[math.floor(x * mapS / 1024)][math.floor(y * mapS / 1024)])
		gr.setBlendMode("subtractive")
		gr.rectangle("fill", (x * mapS) % 1024, (y * mapS) % 1024, mapS, mapS)
		gr.setBlendMode("alpha")
		if t > 0 then
			gr.draw(tiles[t], (x * mapS) % 1024, (y * mapS) % 1024, 0, sizeK, sizeK)
		end
		gr.setCanvas()
	end
end

function tilemap.redraw()
	for x = 0, mapW do
		for y = 0, mapH do
			if x >= 0 and y >= 0 and x <= mapW and y <= mapH then
				t = map[x][y]
				gr.setCanvas(mapC[math.floor(x * mapS / 1024)][math.floor(y * mapS / 1024)])
				gr.setBlendMode("subtractive")
				gr.rectangle("fill", (x * mapS) % 1024, (y * mapS) % 1024, mapS, mapS)
				gr.setBlendMode("alpha")
				if t > 0 then
					gr.draw(tiles[t], (x * mapS) % 1024, (y * mapS) % 1024, 0, sizeK, sizeK)
				end
				gr.setCanvas()
			end
		end
	end
end

function tilemap.get(px, py, r)
	local x, y = px, py
	if r then
		x = math.floor(x / mapS)
		y = math.floor(y / mapS)
	end
	local x, y = math.floor(x), math.floor(y)
	if x >= 0 and y >= 0 and x <= mapW and y <= mapH then
		return x, y, map[x][y]
	else
		return x, y, 0
	end
end