_FONT = gr.newFont("cour.ttf", 10)
_FONTH = _FONT:getHeight("#")
_FONTW = _FONT:getWidth("#")
_FONT:setFilter("nearest", "nearest")

keyDown = kb.isDown

evilib = {}

evilib.debug = {}
evilib.log = {}
evilib.console = {}

evilib.activeDebug = nil

evilib.debug.info = ""
evilib.debug.draw = false

function evilib.debug.setFont(s)
	_FONT = gr.newFont("cour.ttf", s)

	_FONTH = _FONT:getHeight("#")
	_FONTW = _FONT:getWidth("#")
end

function evilib.debug.addMain()				
	evilib.debug.add("<MAIN DATA>")						
	evilib.debug.add("FPS: " .. tm.getFPS())
	evilib.debug.add("DT: " .. tm.getDelta())
	evilib.debug.add("<ADDITIONAL DATA>")
end

function evilib.debug.add(s)
	evilib.debug.info = evilib.debug.info .. s .. "\n"
end
add = evilib.debug.add

function evilib.debug.draw()
	gr.setColor(0, 0, 0)
	gr.print(evilib.debug.info, 1, 1)
	gr.setColor(255, 255, 255)
	gr.print(evilib.debug.info, 0, 0)
end


evilib.log.lines = {}
evilib.log.draw = false

function evilib.log.add(s)
	table.insert(evilib.log.lines, s)
end

function print(str)
	evilib.log.add(str)
	--io.write(str.."\n")
end

function evilib.log.draw()
	local y = 0
	gr.setColor(0, 0, 0, 150)
	gr.rectangle("fill", 0, 0, 800, 20 * _FONTH)
	gr.setColor(255, 255, 255)
	for i = #evilib.log.lines - 19, #evilib.log.lines do
		if evilib.log.lines[i] ~= nil then
			gr.print(">" .. evilib.log.lines[i], 0, y)
			y = y + _FONTH
		end
	end
end

function evilib.drawDebug()
	gr.setColor(255, 255, 255)
	gr.setFont(_FONT)
	if evilib.activeDebug ~= nil then
		evilib.activeDebug.draw()
	end
end


evilib.console.line = ""
evilib.console.log = {}
evilib.console.logLineN = #evilib.console.log
evilib.console.lineCur = 1

function evilib.console.execute()
	local code, err = loadstring(evilib.console.line)
	if evilib.console.line ~= "exit" then
		table.insert(evilib.console.log, evilib.console.line)
		if code ~= nil and not err then
			evilib.log.add(evilib.console.line)
			ok, err2 = pcall(code)
			if not ok then
				evilib.log.add(err2)
			end
			evilib.log.add(tostring(ok))
		else
			evilib.log.add(err)
		end
	else
		love.event.push("quit")
	end
	evilib.console.line = ""
	evilib.console.lineCur = 1
end

function evilib.console.draw()
	evilib.log.draw()
	gr.setColor(0, 0, 0, 150)
	gr.rectangle("fill", 0, wh - _FONTH, 800, _FONTH)
	gr.setColor(255, 255, 255)
	gr.print("CONSOLE>> " .. evilib.console.line, 0, wh - _FONTH)
	gr.print("|", _FONT:getWidth("CONSOLE>> ") + (evilib.console.lineCur - 1 - 0.5) * _FONTW, wh - _FONTH)
end

function evilib.console.textInput(k)
	if evilib.activeDebug ~= evilib.console then return end
	local ec = evilib.console
	local ss = string.sub(ec.line, 1, ec.lineCur - 1)
	local es = string.sub(ec.line, ec.lineCur, #ec.line)
	ec.line = ss .. k .. es
	ec.lineCur = ec.lineCur + 1
end


function evilib.debugUpdate()
	local ed = evilib.debug
	local el = evilib.log
	local ec = evilib.console
	ed.info = ""
	ed.addMain()
	if keyDown("lctrl") then
		if keyPressed("f1") then
			evilib.activeDebug = nil
		end
		if keyPressed("f2") then
			evilib.activeDebug = ed
		end
		if keyPressed("f3") then
			evilib.activeDebug = el
		end
		if keyPressed("f4") then
			evilib.activeDebug = ec
			ec.logLineN = #ec.log + 1
			ec.lineCur = 1
			evilib.keystring = ""
		end
		if keyPressed("v") then
			if evilib.activeDebug == ec then
				ec.line = ec.line .. (love.system.getClipboardText() or "")
			end
		end
	end

	if evilib.activeDebug == ec then
		dt = 0
		if keyPressed("return") then
			ec.execute()
			ec.logLineN = #ec.log + 1
		end
		if keyPressed("backspace") then
			if ec.lineCur > 1 then
				local ss = string.sub(ec.line, 1, ec.lineCur - 2)
				local es = string.sub(ec.line, ec.lineCur, #ec.line)
				ec.line = ss .. es
				ec.lineCur = math.max(ec.lineCur - 1, 1)
			end
		end
		if keyPressed("up") then
			ec.logLineN = ec.logLineN - 1
			ec.logLineN = math.max(1, ec.logLineN)
			if ec.log[ec.logLineN] then
				ec.line = ec.log[ec.logLineN]
			else
				ec.line = ""
			end
			ec.lineCur = #ec.line + 1
		elseif keyPressed("down") then
			ec.logLineN = ec.logLineN + 1
			ec.logLineN = math.min(#ec.log + 1, ec.logLineN)
			if ec.log[ec.logLineN] then
				ec.line = ec.log[ec.logLineN]
			else
				ec.line = ""
			end
			ec.lineCur = #ec.line + 1
		elseif keyPressed("left") then
			ec.lineCur = math.max(ec.lineCur - 1, 1)
		elseif keyPressed("right") then
			ec.lineCur = math.min(ec.lineCur + 1, #ec.line + 1)
		end
		keysPressed = { }
		keysReleased = { }
		buttonsPressed = { }
		buttonsReleased = { }
	end
end




local function error_printer(msg, layer)
	local errstr = debug.traceback(tostring(msg), 1+(layer or 1)):gsub("\n[^\n]+$", "")
    print((debug.traceback("Error: " .. tostring(msg), 1+(layer or 1)):gsub("\n[^\n]+$", "")))
    io.write((debug.traceback("Error: " .. tostring(msg), 1+(layer or 1)):gsub("\n[^\n]+$", "")))
    file = io.open("lastError.log", "w")
    file:write((debug.traceback("Error: " .. tostring(msg), 1+(layer or 1)):gsub("\n[^\n]+$", "")))
    file:flush()
end

function love.errhand(msg)
    msg = tostring(msg)

    error_printer(msg, 1)
    if not love.window or not love.graphics or not love.event then
        return
    end

    if not love.graphics.isCreated() or not love.window.isCreated() then
        if not pcall(love.window.setMode, 800, 600) then
            return
        end
    end

    -- Reset state.
    if love.mouse then
        love.mouse.setVisible(true)
        love.mouse.setGrabbed(false)
    end
    if love.joystick then
        for i,v in ipairs(love.joystick.getJoysticks()) do
            v:setVibration() -- Stop all joystick vibrations.
        end
    end
    if love.audio then love.audio.stop() end
    love.graphics.reset()
    love.graphics.setBackgroundColor(196, 207, 161)
    --local font = love.graphics.setNewFont(16)
    gr.setFont(MainFont)


    local trace = debug.traceback()

    love.graphics.clear()
    love.graphics.origin()

    local err = {}

    local strs = {
    	"Houston, we've got a problem...",
    	"Oh god... it shouldn't have happened.",
    	"Your cat is the cause of this error!\nOh, You don't have a cat?\nOk... it's my error.",
    	"Don't panic! It's not BSoD, just game error."
	}

    table.insert(err, choose(unpack(strs)).."\n")
    table.insert(err, msg.."\n\n")

    for l in string.gmatch(trace, "(.-)\n") do
        if not string.match(l, "boot.lua") then
            l = string.gsub(l, "stack traceback:", "Traceback\n")
            table.insert(err, l)
        end
    end

    local p = table.concat(err, "\n")

    p = string.gsub(p, "\t", "")
    p = string.gsub(p, "%[string \"(.-)\"%]", "%1")



    local size = 4


    local ERRC = gr.newCanvas(1024, 1024)
    local scr = gr.newScreenshot()
    scr = gr.newImage(scr)
    while size < 408 do
    	gr.clear()
    	love.graphics.setColor(255, 255, 255, 255)
    	gr.setCanvas(ERRC)
	    	gr.draw(GAME_CANVAS)
    		drawRectangle("line", 400 - size, 300 - size * (300 / 400), size * 2, size * 2 * (300 / 400))
    	gr.setCanvas()
    	size = size + 10
    	gr.setColor(35, 35, 35, 255)
    	gr.draw(ERRC)
    	gr.setColor(35, 35, 35, 120)
    	gr.draw(ERRC, 2, 2)
    	gr.setColor(255, 255, 255)
    	gr.present()
    end
    gr.setCanvas(ERRC)
    love.graphics.printf(p.."\n\n=========================\nError saved to lastError.log\n=========================", 70, 70, 200, "left", 0, 3, 3)
    gr.setCanvas()

    local function draw()
        love.graphics.clear()
        gr.setColor(35, 35, 35, 255)
    	gr.draw(ERRC)
    	gr.setColor(35, 35, 35, 120)
    	gr.draw(ERRC, 2, 2)
        love.graphics.present()
    end

    while true do
        love.event.pump()

        for e, a, b, c in love.event.poll() do
            if e == "quit" then
                return
            end
            if e == "keypressed" and a == "escape" then
                return
            end
        end

        draw()

        if love.timer then
            love.timer.sleep(0.1)
        end
    end
end