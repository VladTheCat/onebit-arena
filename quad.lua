local Quad = {}
Quad.drawing = false

function Quad.init(ceilWidth, ceilHeight, ceilXCount, ceilYCount)
	Quad.ceilWidth, Quad.ceilHeight = ceilWidth, ceilHeight
	Quad.xcount, Quad.ycount = ceilXCount + 1, ceilYCount + 1
	for x = 0, Quad.xcount do
		Quad[x] = {}
		for y = 0, Quad.ycount do
			Quad[x][y] = {}
		end
	end
end

function Quad.clear()
	for x = 0, Quad.xcount do
		for y = 0, Quad.ycount do
			Quad[x][y] = {}
		end
	end
end

function Quad.addActor(act)
	local quads = {}
	local x1, x2 = math.floor((act.x - act.radius) / Quad.ceilWidth), math.floor((act.x + act.radius) / Quad.ceilWidth)
	local y1, y2 = math.floor((act.y - act.radius) / Quad.ceilHeight), math.floor((act.y + act.radius) / Quad.ceilHeight)
	if inBounds2(x1, y1, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x1][y1])
	end
	if x2 ~= x1 and inBounds2(x2, y1, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x2][y1])
	end
	if y2 ~= y1 and inBounds2(x1, y2, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x1][y2])
	end
	if x2 ~= x1 and y2 ~= y1 and inBounds2(x2, y2, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x2][y2])
	end

	table.foreach(quads, function(i, v) v[act] = true end)
end

function Quad.removeActor(act)
	local quads = {}
	local x1, x2 = math.floor((act.x - act.radius) / Quad.ceilWidth), math.floor((act.x + act.radius) / Quad.ceilWidth)
	local y1, y2 = math.floor((act.y - act.radius) / Quad.ceilHeight), math.floor((act.y + act.radius) / Quad.ceilHeight)
	if inBounds2(x1, y1, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x1][y1])
	end
	if x2 ~= x1 and inBounds2(x2, y1, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x2][y1])
	end
	if y2 ~= y1 and inBounds2(x1, y2, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x1][y2])
	end
	if x2 ~= x1 and y2 ~= y1 and inBounds2(x2, y2, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x2][y2])
	end

	table.foreach(quads, function(i, v) v[act] = nil end)
end

function Quad.getQuadXY(x, y)
	local actors = {}
	local qx, qy = math.floor(x / Quad.ceilWidth), math.floor(y / Quad.ceilHeight)
	if inBounds2(qx, qy, 0, 0, Quad.xcount, Quad.ycount) then
		table.foreach(Quad[qx][qy], function(i, v) table.insert(actors, i) end)
	end
	return actors
end

function Quad.coords(x, y)
	return math.floor(x / Quad.ceilWidth), math.floor(y / Quad.ceilHeight)
end

function Quad.getQuadActor(act)
	local actors = {}
	local quads = {}
	local tempActors = {}
	local x1, x2 = math.floor((act.x - act.radius) / Quad.ceilWidth), math.floor((act.x + act.radius) / Quad.ceilWidth)
	local y1, y2 = math.floor((act.y - act.radius) / Quad.ceilHeight), math.floor((act.y + act.radius) / Quad.ceilHeight)
	if inBounds2(x1, y1, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x1][y1])
	end
	if x2 ~= x1 and inBounds2(x2, y1, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x2][y1])
	end
	if y2 ~= y1 and inBounds2(x1, y2, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x1][y2])
	end
	if x2 ~= x1 and y2 ~= y1 and inBounds2(x2, y2, 0, 0, Quad.xcount, Quad.ycount) then
		table.insert(quads, Quad[x2][y2])
	end

	table.foreachi(quads, function(i, v)
		table.foreach(v, function(k, vv)
			if k ~= act then
				if not hasActor(tempActors, k) then
					table.insert(actors, k) 
					table.insert(tempActors, k)
				end
			end
		end)
	end)
	return actors
end

function Quad.getQuadCamera()
	local actors = {}
	local tempActors = {}
	local startX, startY = math.floor((player.x - 400 - camDX) / Quad.ceilWidth), math.floor((player.y - 300 - camDY) / Quad.ceilHeight)
	local endX, endY = math.floor((player.x + 400 - camDX) / Quad.ceilWidth), math.floor((player.y + 300 - camDY) / Quad.ceilHeight)
	add(string.format("Start: (%d; %d)\nEnd: (%d; %d)", startX, startY, endX, endY))
	for x = startX, endX do
		for y = startY, endY do
			if inBounds2(x, y, 0, 0, Quad.xcount, Quad.ycount) then
				table.foreach(Quad[x][y], function(k, v)
					if k ~= act then
						if not hasActor(tempActors, k) then
							table.insert(actors, k) 
							table.insert(tempActors, k)
						end
					end
				end)
			end
		end
	end
	return actors
end

function Quad.draw()
	if Quad.drawing then
		for x = 0, Quad.xcount do
			for y = 0, Quad.ycount do
				gr.setColor(10, 100, 200)
				gr.rectangle("line", x * Quad.ceilWidth, y * Quad.ceilHeight, Quad.ceilWidth, Quad.ceilHeight)
				local count = 0
				table.foreach(Quad[x][y], function(i, v) count = count + 1 end )
				gr.print(count, x * Quad.ceilWidth + 10, y * Quad.ceilHeight + 10)
			end
		end
	end
end


return Quad