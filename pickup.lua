Pickup = {}

function Pickup.place(t, x, y)
	local p = {}
	p.type = t
	p.x = x
	p.y = y
	p.time = 0
	p.action = Pickup[t].action
	p.draw = Pickup.draw
	p.img = Player.Buff.Img[t]
	p.update = Pickup.update
	p.solid = true
	p.radius = 14
	Pickups = Pickups + 1
	table.insert(activities, p)
end

Pickup["speedup"] = {}
Pickup["speedup"].action = function(act)
	Player.buff("speedup", 10)
end

Pickup["regen"] = {}
Pickup["regen"].action = function(act)
	player.hp = player.hp + 16
	SndBuff:play()
end

Pickup["skill"] = {}
Pickup["skill"].action = function(act)
	Player.buff("skill", 10)
end

Pickup["invinc"] = {}
Pickup["invinc"].action = function(act)
	Player.buff("invinc", 15)
end

Pickup["time"] = {}
Pickup["time"].action = function(act)
	Player.buff("time", 10)
end

function Pickup.draw(act)
	gr.rectangle("fill", act.x - 14, act.y - 14, 28, 28)
	gr.setBlendMode("subtractive")
	gr.draw(act.img, act.x, act.y, 0, 3, 3, 3, 3)
	gr.setBlendMode("alpha")
end

function Pickup.update(act, dt)
	Quad.addActor(act)
	if math.sqrt((act.x - player.x)^2 + (act.y - player.y)^2) < act.radius + player.radius + 5 and not player.dead then
		table.insert(activitiesRemove, act)
		Pickups = Pickups - 1
		act.action(act)
		if act.type ~= "regen" then
			if not ShowedTutor.pickup then
				ShowTutorText(TutorText.pickup)
				ShowedTutor.pickup = true
			end
		else
			if not ShowedTutor.health then
				ShowTutorText(TutorText.health)
				ShowedTutor.health = true
			end
		end
	end
end