local usersFile = love.filesystem.newFile("users.dat")
local scoreFile = love.filesystem.newFile("scores.dat")

function hasUser(user, token)
	local has = false
	local differentToken = false
	local users = getUsers()
	
	for i, v in pairs(users) do
		if i == user then
			has = true
			if v ~= token then
				differentToken = true
			end
			break
		end
	end
	return has, differentToken
end

function getUsers()
	local users = {}
	local ok = usersFile:open("r")
	if ok then
		for line in usersFile:lines() do
			local login, pass = string.match(line, "(%a+)%s*:%s*(%a+)")
			if login and pass then
				users[login] = pass
			end
		end
		usersFile:close()
	end
	return users
end

function GJ.addUser(name, token)
	local users = getUsers()
	local has, diff = hasUser(name, token)
	if not has or (has and diff) then
		users[name] = token
	end
	usersFile:open("w")
	for i, v in pairs(users) do
		usersFile:write(string.format("%s : %s\n", i, v))
		print(i.." : "..v)
	end
	usersFile:flush()
	usersFile:close()
end

function GJ.authUserLocal(name, token)
	usersFile:open("r")
	for line in usersFile:lines() do
		local login, pass = string.match(line, "(%a+)%s*:%s*(%a+)")
		if login and pass then
			if login == name and pass == token then
				GJ.isLoggedIn = true
				GJ.username = name
				GJ.userToken = token
				break
			end
		end
	end
	usersFile:close()
	return GJ.isLoggedIn
end

function GJ.addScoreLocal(url)
	local str = ""
	for i = 1, #url do
		local b = string.byte(url, i)
		b = bit.bxor(b, 0xf4124ab)
		local s = string.char(b)
		str = str .. s
	end
	scoreFile:open("a")
	scoreFile:write(str.."\n")
	scoreFile:flush()
	scoreFile:close()
end

function GJ.pushLocalScore()
	local scores = {}
	local nextScores = ""
	scoreFile:open("r")
	for line in scoreFile:lines() do
		http.request(line)
	end
	scoreFile:open("w")
	scoreFile:write()
	scoreFile:flush()
	scoreFile:close()
end