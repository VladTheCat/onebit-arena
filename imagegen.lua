function createImage(tab, w, h)
	local width, height = #tab[1], #tab
	local id = love.image.newImageData(width, height)
	for y = 1, height do
		local str = tab[y]
		for x = 1, width do
			local char = string.sub(str, x, x)
			if char == "0" then
				id:setPixel(x - 1, y - 1, 255, 255, 255, 255)
			end
		end
	end
	local img = love.graphics.newImage(id)
	img:setFilter("linear", "nearest")
	return img
end