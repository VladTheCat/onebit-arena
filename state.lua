State = {}
local self

--[[ GAME

00000000  000000  00      00  000000
00		  00  00  0000  0000  00
00  0000  00  00  00  00  00  0000
00    00  000000  00      00  00
00000000  00  00  00      00  000000

]]

State.game = {}

GameTable = DiskTables[1]


self = State.game

function State.game.load()
	mapWidth, mapHeight = 81, 81
	tilemap.createNew(mapWidth, mapHeight)
	tilemap.generateSymmetric()
	Quad.init(200, 200, 81 * mapS / 200, 81 * mapS / 200)
	activities = {}
	activitiesRemove = {}
	Player.set(mapW / 2 * mapS + 16, mapH / 2 * mapS + 16)
	disk.init("disk")

	DIFFICULT = 1

	enemyTimer = 0.01	
	spawnRate = 5 
	spawnAlarm = 2 
	enemies = 0 
	boss = 0 			
	Pickups = 0
	camDX, camDY = 0, 0
	camX, camY = 0, 0
	Time = 0
	Score = 0
	Kills = 0
	Time1 = 0
	Time2 = 0
	BuffTime = 0
	Score1 = 0
	Score2 = 0
	Kills1 = 0
	Kills2 = 0
	TimeK = 1

	GUIComboTimer = 0
	GUIComboString = ""
	-- EndScore = 0
	Scoretables = {}
	ScoretablesRemove = {}

	Wave = {
		{
			{e = 1, count = 7, score = 0},
			{e = 3, count = 2, score = 2500},
			{e = 5, count = 5, score = 5000},
			{e = 4, count = 1, score = 7500},
			{e = 2, count = 2, score = 12500},
			{e = 6, count = 2, score = 15000},
			{e = 7, count = 2, score = 17500},
			{e = 0, count = 1, score = 20000}
		}
	}
	EnemyTable = {}
	if GameMode == "time" then
		EnemyTable = {1,1,1,1,1,1,1,1,1,1,1,3,3,5,5,5,5,4,2,2,6,6,7,7}
	end
	currentWave = 1
	for i = 1, Wave[1][currentWave].count do
		table.insert(EnemyTable, Wave[DIFFICULT][currentWave].e)
	end
	currentWave = 2
	NETextTime = 0
	BTextTime = 0
	HealthWarning = 0

	for i = 1, 12 do
		Music[i]:stop()
	end

	currentTrack = math.random(7)
	ChoosenDisk = DiskStates[DiskState]
	--DebugPlaceEnemies()
end

local count, t, average, total = 0, 0, 0, 0
function State.game.update(delta)
	Quad.clear()
	if disk.action == "frisbee" then
		if not ShowedTutor.frisbee2 and not ShowedTutor.frisbee1 then
			ShowTutorText(TutorText.frisbee2)
			ShowedTutor.frisbee1 = true
			ShowedTutor.frisbee2 = true
			return
		end
	end
	if not ShowedTutor.move then	
		ShowTutorText(TutorText.move)
		ShowedTutor.move = true
		return
	end
	checkCheat = false

	dt = delta * TimeK

	BuffTime = BuffTime + dt
	GUIComboTimer = GUIComboTimer - dt

	HealthWarning = HealthWarning + delta
	if HealthWarning > 0.5 then
		HealthWarning = 0
	end

	if keyPressed("escape") and not player.dead then
		Music[currentTrack]:pause()
		state = State.pause
		state.load(true)
	end
	
	spawnRate = 5 + 3 * (DIFFICULT - 1)
	if GameMode == "hardcore" then
		spawnRate = 4 + 2 * (DIFFICULT - 1)
	end
	spawnAlarm = math.max(0.7, 2 - 0.2 * DIFFICULT)
	if boss ~= 0 then
		spawnRate = 1
		spawnAlarm = 5
	end

	if GameMode == "time" then
		player.hp = player.hp - (4 +(Time / 100)) * dt
	end

	if not player.dead then
		if boss == 0 then
			Time = Time + dt
			Time1 = Time1 - dt
			Time2 = tostring(tonumber(Time2) - dt)
		end
		if Music[currentTrack]:isStopped() and PLAYMUSIC then
			local prevTrack = currentTrack
			while currentTrack == prevTrack do
				currentTrack = math.random(7)
				if boss == 1 then
					currentTrack = math.random(8, 12)
				end
			end
			Music[currentTrack]:play()
		end
	end

	NETextTime = NETextTime - dt
	BTextTime = BTextTime - dt

	if Time > 0 and Score > 0 and Kills > 0 then
		if not ((Score == -Score1 and Score == -tonumber(Score2)) and
				(math.floor(Time) == math.floor(-Time1) and math.floor(Time) == math.floor(-tonumber(Time2))) and
				(Kills == -Kills1 and Kills == -tonumber(Kills2))) then
			checkCheat = true
		end
	end
	if currentWave < #Wave[1] and GameMode ~= "time" then
		if Score >= Wave[1][currentWave].score + 30000 * (DIFFICULT - 1) then
			for i = 1, Wave[1][currentWave].count do
				table.insert(EnemyTable, Wave[1][currentWave].e)
			end
			currentWave = currentWave + 1
			NETextTime = 2
			if not ShowedTutor.newEnemy then
				ShowTutorText(TutorText.newEnemy)
				ShowedTutor.newEnemy = true
			end
		end
	else
		if Score >= Wave[1][currentWave].score + 25000 * (DIFFICULT - 1) and GameMode ~= "hardcore" then
			spawnRate = 1
			spawnAlarm = 3
			if boss == 0 then
				for i, v in ipairs(activities) do
					if v.actor == "enemy" then
						v.hp = 0
						v.getScore = false
					end
				end
				math.randomseed(os.clock())
				local b = math.random(3)
				BOSS = Boss[b].place(-100, -100)

				player.hp = 100

				boss = 1
				BTextTime = 2
			end
		end
	end

	if math.floor(BuffTime % 15) == 14 and Pickups < 20 and SpawnPickups then
		local x = math.random(2, mapW - 2)
		local y = math.random(2, mapH - 2)
		local _, __, t = tilemap.get(x, y)
		while t == 2 or t == 3 do
			x = math.random(2, mapW - 2)
			y = math.random(2, mapH - 2)
			_, __, t = tilemap.get(x, y)
		end
		local PickupTable = {"speedup", "speedup", "speedup", "speedup",
							"skill", "skill", "skill", "skill", 
							"regen", "regen", "invinc", "time"}
		if GameMode == "hardcore" then
			PickupTable = {"speedup", "speedup", "speedup", "speedup",
							"skill", "skill", "skill", "skill", 
							"invinc", "time"}
		end
		Pickup.place(choose(unpack(PickupTable)), x * mapS + mapS / 2, y * mapS + mapS / 2)
	end

	enemyTimer = enemyTimer - dt
	--[[if enemyTimer <= 0 and SpawnEnemies then
		for i = 1, math.random(spawnRate) do
			if enemies < 60 then
				enemy = choose(unpack(EnemyTable))
				if checkCheat then
					enemy = 6
				end
				local distance = 550
				local dir = math.random() * pi * 2
				local px, py = player.x + distance * cos(dir), player.y + distance * sin(dir)
				while px < 40 or py < 40 or px > mapW * mapS - 40 or py > mapH * mapS - 40 do
					distance = 550
					dir = math.random() * pi * 2
					px, py = player.x + distance * cos(dir), player.y + distance * sin(dir)
				end
				Enemy.place(enemy, px, py)
				enemies = enemies + 1
			end
		end
		enemyTimer = spawnAlarm
	end]]
	if enemies < 100 and SpawnEnemies then
		enemy = choose(unpack(EnemyTable))
		local distance = 550
		local dir = math.random() * pi * 2
		local px, py = player.x + distance * cos(dir), player.y + distance * sin(dir)
		while px < 40 or py < 40 or px > mapW * mapS - 40 or py > mapH * mapS - 40 do
			dir = math.random() * pi * 2
			px, py = player.x + distance * cos(dir), player.y + distance * sin(dir)
		end
		Enemy.place(enemy, px, py)
		enemies = enemies + 1
	end


	--[[for index, object in ipairs(activities) do
		object.update(object, dt)
		if object.actor == "enemy" then
			if object.distToPlayer < 300 then
				if not ShowedTutor.shoot then
					ShowTutorText(TutorText.shoot)
					ShowedTutor.shoot = true
					return
				end
			end
		end
	end]]
	tic()
	table.foreachi(activities, updateActivities)
	t = toc()
	total = total + t
	count = count + 1
	average = total / count
	add("Average time: "..average)

	for i, v in ipairs(Scoretables) do
		v.y = v.y - 50 * dt
		v.time = v.time - dt
		if v.time <= 0 then
			table.insert(ScoretablesRemove, v)
		end
	end
	disk.update(dt)

	for i, v in ipairs(activitiesRemove) do
		for ii, vv in ipairs(activities) do
			if vv == v then
				table.remove(activities, ii)
			end
		end
	end
	for i, v in ipairs(ScoretablesRemove) do
		for ii, vv in ipairs(Scoretables) do
			if vv == v then
				table.remove(Scoretables, ii)
			end
		end
	end

	if disk.combo > 0 or (disk.strike > 0 and disk.throwed) then
		local str = disk.strike
		if disk.combo > 0 then
			str = disk.combo
		end
		local comboStr = "CMB x"..str
		if disk.comboTime >= 0.1 then
			local time = round(disk.comboTime, 1)
			if time == math.floor(time) then
				time = time .. ".0"
			end
			comboStr = comboStr..":"..time
		end
		GUIComboString = comboStr
		GUIComboTimer = 2
	end
	

	camX, camY = (400 - mx) / 3, (300 - my) / 3
	camDX, camDY = camDX + (camX - camDX) / 10, camDY + (camY - camDY) / 10
end

function updateActivities(i, v)
	local dt = love.timer.getDelta() * TimeK
	v.update(v, dt)
	if v.actor == "enemy" then
		if v.distToPlayer < 300 then
			if not ShowedTutor.shoot then
				ShowTutorText(TutorText.shoot)
				ShowedTutor.shoot = true
				return
			end
		end
	end
end

function State.game.draw()
	gr.push()
		gr.translate(math.floor(-player.x + 400 + camDX), math.floor(-player.y + 300 + camDY))
		for x = 0, #mapC do
			for y = 0, #mapC[1] do
				gr.draw(mapC[x][y], x * 1024, y * 1024)
			end
		end

		--[[for index, object in ipairs(activities) do
			object.draw(object)
		end]]
		local drawingActors = Quad.getQuadCamera()
		table.foreachi(drawingActors, function(i, v) v.draw(v) end)
		gr.draw(disk.img[disk.action] or disk.img["disk"], disk.x, disk.y, disk.angle, sizeK, sizeK, 3, 3)

		gr.rectangle("fill", -100000, -100000, 300000, 100000)
		gr.rectangle("fill", -100000, mapH * mapS + mapS - 1, 300000, 100000)
		gr.rectangle("fill", -100000, 0, 100000, mapH * mapS + mapS)
		gr.rectangle("fill", mapW * mapS + mapS - 1, 0, 100000, mapH * mapS + mapS)

		if disk.throwed then
			local dir = math.atan2(disk.y - player.y, disk.x - player.x)
			local x, y = player.x + 40 * cos(dir), player.y + 40 * sin(dir)
			gr.setBlendMode("subtractive")
			gr.draw(Arrow, x, y, dir, 6, 6, 1, 2.5)
			gr.setBlendMode("alpha")
			gr.draw(Arrow, x, y, dir, 4, 4, 1, 2.5)
		end

		for i, v in ipairs(Scoretables) do
			gr.rectangle("fill", v.x - v.textw - 4, v.y - v.texth - 4, v.textw * 2 + 8, v.texth * 2 + 8)
			gr.setBlendMode("subtractive")
			gr.print(v.text, v.x - v.textw, v.y - v.texth, 0, 2, 2)
			gr.setBlendMode("alpha")
		end
	gr.pop()

	if state == State.game then
		local h = math.floor((player.hp / 100 * 184) / 4) * 4
		local s = math.floor((player.skill / 100 * 184) / 4) * 4
		drawRectangle("line", 0, 556, 200, 44)		-- HEALTH RECT
		drawRectangle("line", 600, 556, 200, 44)	-- SKILL RECT
		drawRectangle("line", 100, 0, 600, 44)		-- SCORE RECT
		drawRectangle("line", 320, 556, 160, 44)	-- TIME RECT
		local kll = StringZero(Kills, 5)
		local scr = StringZero(Score, 8)
		if GameMode == "time" then
			kll = "-----"
			scr = "--------"
		end
		local t = "Kills: "..kll.."  Score: "..scr
		printC(t, 400, 22, 4)
		t = SecondsToTime(Time)
		printC(t, 400, 580, 4)
		if player.hp > 0 then
			drawRectangle("fill", 8, 564, h, 28)
		end
		if player.skill > 0 then
			drawRectangle("fill", 608 + (184 - s), 564, s, 28)
		end
		gr.setBlendMode("subtractive")
		printC("HEALTH", 100, 580, 4)
		printC("SKILL", 700, 580, 4)
		gr.setBlendMode("alpha")
		if player.hp < 20 and player.hp > 0 and HealthWarning > 0.25 then
			local t = "WARNING"
			if GameMode == "hardcore" then
				t = "1 HP"
			end
			printC(t, 100, 580, 4)
		end

		if player.buff then
			local s = 44
			local x = 200
			local y = 556
			drawRectangle("line", 380, 600 - 40 - 40, 40, 40)
			if player.buffTime > 3 or player.buffTime % 0.5 < 0.25 then
				gr.draw(player.buff.img, 400, 600 - 40 - 20, 0, 4, 4, 3, 3)
			end
			gr.setBlendMode("alpha")
		end

		if NETextTime > 0 or BTextTime > 0 then
			drawRectangle("line", 540, 516 - 50, 260, 44)
			local t = "NEW ENEMY"
			if BTextTime > 0 then
				t = "WARNING"
			end
			if (NETextTime > 0 and math.floor(NETextTime * 5) % 2 == 0) or
			   (BTextTime > 0 and math.floor(BTextTime * 5) % 2 == 0) then
			   printC(t, 670, 540 - 50, 4)
			end
		end
		if GUIComboTimer > 0 and GameMode ~= "time" then
			drawRectangle("line", 0, 516 - 50, 260, 44)
			printC(GUIComboString, 130, 540 - 50, 4)
		end
		if boss == 1 then
			local w = (500 - 14) / 2
			local hpb = math.floor(BOSS.hp / BOSS.maxHP * w / 4) * 4
			drawRectangle("line", 150, 40, 500, 24)
			drawRectangle("fill", 400 - hpb, 48, hpb * 2, 8)
		end
	end

	--add(#Quad.getQuadActor(player))

end

--[[ INTRO

000000  00    00  000000  000000  000000
  00    0000  00    00    00  00  00  00
  00    00  0000    00    0000    00  00
  00    00    00    00    00  00  00  00
000000  00    00    00    00  00  000000

]]

State.intro = {}

self = State.intro

function State.intro.load()
	self.time = 4
end

function State.intro.update(dt)
	self.time = self.time - dt
	if self.time <= 0 or mousePressed("l") then
		state = State.login
		State.login.goto = State.menu
		state.load()
	end
	FirstLogin = true
end

function State.intro.draw()
	gr.rectangle("fill", 0, 0, 800, 600)
	gr.setBlendMode("subtractive")
	gr.draw(IntroImage, 90, 80, 0, 8, 8, 8, 8)
	gr.print(IntroText, 20, 180, 0, 4, 4)
	gr.print(NameText, 400 - NameTextW, 290, 0, 8, 8)
	gr.draw(LoveImage, 756, 540, 0, 4, 4, 5, 5)
	printC("l&ve", 756, 578, 4)
	gr.print(Ver, 4, 570, 0, 4, 4)
	gr.setBlendMode("alpha")
end

--[[ LOGIN 

00      000000  000000  000000  00    00
00      00  00  00        00    0000  00
00      00  00  00  00    00    00  0000
00      00  00  00  00    00    00    00
000000  000000  000000  000000  00    00

]]

State.login = {}
State.login.goto = State.menu

self = State.login

function State.login.load()
	local self = State.login

	self.fields = {}
	self.fields["name"] = ""
	self.fields["token"] = ""
	if GJ.isLoggedIn then
		self.fields["name"] = GJ.username
		self.fields["token"] = GJ.userToken
	end
	self.activeInput = "name"
	self.input = true
	self.success = false
	self.guest = false
	self.alarm = -1
end

function State.login.update(dt)
	local self = State.login
	local login = false
	if self.input then
		if keyPressed("return") then
			if self.fields["name"] ~= "" and self.fields["token"] ~= "" then
				self.success = GJ.authUser(self.fields["name"], self.fields["token"])
				if self.success then
					GJ.addUser(self.fields["name"], self.fields["token"])
				else
					self.success = GJ.authUserLocal(self.fields["name"], self.fields["token"])
				end
				self.alarm = 2
				self.input = false
				login = true
			else
				if self.activeInput == "name" then
					self.activeInput = "token"
				else
					self.input = false
				end
			end
		end
		if keyPressed("backspace") then
			self.fields[self.activeInput] = string.sub(self.fields[self.activeInput], 1, #self.fields[self.activeInput] - 1)
		end
		if keyPressed("up") or keyPressed("down") then
			if self.activeInput == "name" then
				self.activeInput = "token"
			else
				self.activeInput = "name"
			end
		end
		if mousePressed("l") then
			if my > 174 and my < 174 + 24 then
				self.activeInput = "name"
			elseif my > 174 + 24 and my < 174 + 24 * 2 then
				self.activeInput = "token"
			elseif my > 600 - GJimage:getHeight() * 4 then
				openLink("http://gamejolt.com/")
			end
		end
		if keyPressed("escape") then
			self.input = false
			self.guest = true
			self.alarm = 2
			GJ.isLoggedIn = false
		end
	else
		if dt < 0.2 then
			self.alarm = self.alarm - dt
		end
		if self.alarm <= 0 or keyPressed("return") or keyPressed(" ") or mousePressed("l") or keyPressed("escape") then
			if self.success or self.guest then
				if self.success then
					GJ.openSession()
				end
				state = State.translate
				self.goto.load()
				state.load(self, self.goto, true, true)
				if FirstLogin then
					SndGS:play()
					FirstLogin = false
				end
			else
				self.fields["name"] = ""
				self.fields["token"] = ""
				self.activeInput = "name"
				self.input = true
				self.success = false
				self.alarm = -1
			end
		end
	end
end

function State.login.draw()
	local self = State.login
	gr.rectangle("fill", 0, 0, 800, 600)
	gr.setBlendMode("subtractive")
	local iw = GJimage:getWidth() * 4
	local tw = GJtext:getWidth() * 8
	local w = (iw + tw + 4) / 2
	local k = iw / tw
	gr.draw(GJimage, 400 - w, 600 - GJimage:getHeight() * 4 - 4, 0, 4, 4)
	gr.draw(GJtext, 400 - w + iw + 4, 600 - GJimage:getHeight() * 4 - 4, 0, 8, 8)
	local text = "Please, login"
	if GJ.isLoggedIn then
		text = "You're logged in ("..GJ.username..")"
	end
	printC(text, 400, 158, 4)
	local x, y = 400, 246
	text = "or press Escape to enter as guest."
	if not self.input then
		text = "Successful!"
		if not self.success then
			text = "Error! Try again."
		end
		if self.guest then
			text = "Entering as guest..."
		end
	end
	printC(text, x, y, 4)
	gr.rectangle("fill", 100, 170, 600, 24 * 2 + 12)
	gr.setBlendMode("alpha")
	for i, v in ipairs({"name", "token"}) do
		local text = self.fields[v]
		if text == "" then text = v end
		if self.activeInput == v then
			text = "[= "..text.." =]"
		end
		printC(text, 400, 178 + i * 28 - 18, 4)
	end
end

--[[ CREDITS

000000  000000  000000  0000    000000  000000  000000
00      00  00  00      00  00    00      00    00
00      0000    0000    00  00    00      00    000000
00      00  00  00      00  00    00      00        00
000000  00  00  000000  0000    000000    00    000000

]]

State.credits = {}

self = State.credits

function State.credits.load()
end

function State.credits.update(dt)
	if mousePressed("l") or keyPressed("return") or keyPressed(" ") or keyPressed("escape") then
		if my > 600 - GJimage:getHeight() * 4 then
			if mx > 100 and mx < 700 then
				openLink("http://gamejolt.com/")
			elseif mx > 720 then
				openLink("http://love2d.org/")
			end
		end
	end
	BackButton.update(State.credits.exitFunc)
end

function State.credits.exitFunc()
	state = State.translate
	state.load(self, State.menu, false, true)
end

function State.credits.draw()
	gr.rectangle("fill", 0, 0, 800, 600)
	gr.setBlendMode("subtractive")
	printC("CREDITS", 400, 28, 8)
	gr.print([[MAIN CODE/GRAPHICS/SOUNDS:
VladTheCat

ONLINE RECORDS SERVICE:
@insweater
Norman Rosewood

MUSIC:
Valera Nagato

TESTING:
Baron Von Dugzer, Slava Shilov,
Pavel Pushkaryov,  YooPita,
Denis Grintsevich, 
and all followers of my public
]], 4, 100, 0, 4, 4)
--gr.print("AaBbCcDdEeFfGgHhIiJjKkLl\nMmNnOoPpQqRrSsTtUuVvWwXxYyZz\n\nABCDEFGHIJKLMNOPQRSTUVWXYZ\nabcdefghijklmnopqrstuvwxyz\n1234567890", 4, 100, 0, 4, 4)
	gr.draw(LoveImage, 756, 540, 0, 4, 4, 5, 5)
	printC("l&ve", 756, 578, 4)
	gr.print(Ver, 4, 570, 0, 4, 4)
	local x = 280
	local iw = GJimage:getWidth() * 4
	local tw = GJtext:getWidth() * 8
	local w = (iw + tw + 4) / 2
	local k = iw / tw
	local xoff = -30
	gr.draw(GJimage, 400 - w + xoff, 600 - GJimage:getHeight() * 4 - 4, 0, 4, 4)
	gr.draw(GJtext, 400 - w + iw + 4 + xoff, 600 - GJimage:getHeight() * 4 - 4, 0, 8, 8)
	gr.setBlendMode("alpha")
	BackButton.draw()
end

--[[ MENU

00      00  000000  00    00  00  00
0000  0000  00      0000  00  00  00
00  00  00  0000    00  0000  00  00
00      00  00      00    00  00  00
00      00  000000  00    00  000000

]]

State.menu = {}
	State.menu.x = 0
	State.menu.y = 180
	State.menu.yof = 28
	State.menu.choosen = 0
	State.menu.text = {"START", "NEW MAP", "HIGHSCORES", "SETTINGS", "CREDITS", "LOGIN", "EXIT"}
	
function State.menu.load(p)
	local self = State.menu

	local w = {}
	local h = 0
	for i, v in ipairs(State.menu.text) do
		w[i] = MainFont:getWidth(v) * 4
		h = h + MainFont:getHeight(v) * 4 + 4
	end
	self.maxX = 254--math.max(unpack(w))
	self.height = #self.text * h * 4 + 12
	self.y = 300 - h / 2
	self.move = 1
	self.moveSpeed = 400
	self.pause = p
	local mwidth = 0
end

function State.menu.update(dt)
	if FIRSTPLAY then
		if not showSplashQuestion("Do you want to see tutorial?", 400, 450, "center", "top") then
			ShowTutor(false)
		end
		FIRSTPLAY = false
	end

	local self = State.menu
	if keyPressed("escape") and self.pause then
		state = State.game
		if PLAYMUSIC then Music[currentTrack]:play() end
	end
	self.choosen = 0
	if mx > self.x and mx < self.x + self.maxX + 14 then
		for i = 1, #self.text do
			if my > self.y + i * self.yof and my < self.y + i * self.yof + self.yof then
				self.choosen = i
				break
			end
		end
	end

	add(self.y + self.height)
	add(my)
	if mousePressed("l") then
		SndClick:play()
		if self.choosen == 1 then
			state = State.modeMenu
			state.load()
		elseif self.choosen == 2 and not self.pause then
			tilemap.generateSymmetric()
		elseif self.choosen == 1000 and self.pause then
			state = State.translate
			state.load(State.menu, State.endScreen, true)
			self.pause = false
		elseif self.choosen == 3 then
			--state = State.translate
			state = State.highMenu
			state.load()
			--state.load(State.menu, State.highscores, true)
		elseif self.choosen == 4 then
			state = State.settings
			state.parent = State.menu
			state.load()
		elseif self.choosen == 5 then
			state = State.translate
			state.load(State.menu, State.credits, false)
		elseif self.choosen == 6 then
			state = State.translate
			State.login.goto = State.menu
			state.load(State.menu, State.login, true)
		elseif self.choosen == 7 then
			local result = showSplashQuestion("Are you sure?", 400, 300, "center", "center")
			if result then love.event.push("quit") end
		end
	end
end

function State.menu.draw()
	local self = State.menu
	State.game.draw()
	local h = MainFont:getHeight("W")
	drawRectangle("line", self.x, self.y - 8 + self.yof, self.maxX + 14, #self.text * h * 4 + 12)
	for i = 1, #self.text do
		local t = self.text[i]
		if self.choosen == i then
			gr.rectangle("fill", 4 + self.x, self.y + i * self.yof, self.maxX + 8, self.yof - 4)
			gr.setBlendMode("subtractive")
		end
		gr.print(t, 10 + self.x, self.y + i * self.yof, 0, 4, 4)
		gr.setBlendMode("alpha")
	end
	local t = "ONE BIT ARENA"
	local w, h = MainFont:getWidth(t) * 10, MainFont:getHeight(t) * 10
	drawRectangle("line", 400 - w / 2 - 3, 0, w + 6, h + 6)
	printC(t, 400, h / 2 + 8, 10)
	if GJ.isLoggedIn then
		t = "You're logged in as \""..GJ.username.."\""
		w, h = MainFont:getWidth(t) * 4, MainFont:getHeight(t) * 4
		drawRectangle("line", 800 - w - 12, 600 - h - 12, w + 12, h + 12)
		printC(t, 800 - w / 2 - 6, 600 - h / 2 - 4, 4)
	end
end

--[[

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

]]

State.settings = {}
	State.settings.x = 264
	State.settings.y = 250
	State.settings.yof = 28
	State.settings.choosen = 0
	State.settings.maxX = 340

function State.settings.load()
	local h = MainFont:getHeight("W")
	local self = State.settings
	self.variant = {}
	self.variant[true] = "ON"
	self.variant[false] = "OFF"
	self.menu = { buttons = { "SOUND", "MUSIC", "FULLSCREEN", "BACK"}, params = {}}
	self.height = #self.menu.buttons * h * 4 + 12
	local m = self.menu
	if love.audio.getVolume() == 1 then
		m.params[1] = true
	else
		m.params[1] = false
	end
	m.params[2] = PLAYMUSIC
	m.params[3] = love.window.getFullscreen()
end

function State.settings.update(dt)
	local self = State.settings
	self.choosen = 0
	if mx > self.x and mx < self.x + self.maxX + 14 then
		for i = 1, #self.menu.buttons do
			if my > self.y + i * self.yof and my < self.y + i * self.yof + self.yof then
				self.choosen = i
				break
			end
		end
	end

	local tab = {[true] = "1", [false] = "0"}
	local set1, set2, set3 = tostring(love.audio.getVolume()), tab[PLAYMUSIC], tab[love.window.getFullscreen()]

	if mousePressed("l") then
		SndClick:play()
		local file = love.filesystem.newFile("settings")		
		if self.choosen == 1 then
			if self.menu.params[1] == true then
				self.menu.params[1] = false
				love.audio.setVolume(0)
			else
				self.menu.params[1] = true
				love.audio.setVolume(1)
			end
			set1 = tostring(love.audio.getVolume())
		elseif self.choosen == 2 then
			PLAYMUSIC = not PLAYMUSIC
			self.menu.params[2] = PLAYMUSIC
			set2 = tab[PLAYMUSIC]
		elseif self.choosen == 3 then
			love.window.setFullscreen(not love.window.getFullscreen())
			self.menu.params[3] = love.window.getFullscreen()
			set3 = tab[love.window.getFullscreen()]
		elseif self.choosen == 4 or self.choosen == 0 then
			state = self.parent
		end
		if self.choosen < 4 and self.choosen > 0 then
			file:open("w")
			file:write(set1..set2..set3)
			file:flush()
			file:close()
		end
	end
end

function State.settings.draw()
	local self = State.settings
	self.parent.draw()
	local h = MainFont:getHeight("W")
	drawRectangle("line", self.x, self.y - 8 + self.yof, self.maxX + 14, #self.menu.buttons * h * 4 + 12)
	for i = 1, #self.menu.buttons do
		local t = self.menu.buttons[i]
		if i < 4 then
			t = t .. ": "..self.variant[self.menu.params[i]]
		end
		if self.choosen == i then
			gr.rectangle("fill", self.x, self.y + i * self.yof, self.maxX + 10, self.yof - 4)
			gr.setBlendMode("subtractive")
		end
		gr.print(t, 10 + self.x, self.y + i * self.yof, 0, 4, 4)
		gr.setBlendMode("alpha")
	end
end

--[[

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

]]
State.modeMenu = {}
	State.modeMenu.x = 264
	State.modeMenu.y = State.menu.y
	State.modeMenu.yof = 28
	State.modeMenu.choosen = 0
	State.modeMenu.maxX = 300

function State.modeMenu.load()
	local h = MainFont:getHeight("W")
	State.modeMenu.y = State.menu.y
	local self = State.modeMenu
	self.menu = {buttons = {}}
	local p = 1
	for i = 1, #Modes do
		self.menu.buttons[i] = string.upper(ModeName[i])
		p = i
	end
	p = p + 1
	local m = self.menu
	m.buttons[#Modes + 1] = "BACK"

	self.height = #self.menu.buttons * h * 4 + 12
end


function State.modeMenu.update(dt)
	local self = State.modeMenu
	self.choosen = 0
	add(self.y + self.height)
	add(my)
	if mx > self.x and mx < self.x + self.maxX + 14 then
		for i = 1, #self.menu.buttons do
			if my > self.y + i * self.yof and my < self.y + i * self.yof + self.yof then
				self.choosen = i
				break
			end
		end
	end
	if mousePressed("l") then
		SndClick:play()
		if self.choosen == 1 then
			state = State.diskMenu
			state.load()
			GameMode = "classic"
		elseif self.choosen == 2 then
			ChoosenDisk = "frisbee"
			DiskState = self.choosen
			disk.init(ChoosenDisk)
			state = State.game
			if PLAYMUSIC and not player.dead then Music[currentTrack]:play() end
			State.menu.pause = false
			started = true
			player.hp = 1
			GameMode = "hardcore"
			GameTableName = "hardcore"
			GameTable = ModeTables[GameMode]
			--tilemap.generateEmpty()
		elseif self.choosen == 3 then
			ChoosenDisk = "disk"
			DiskState = self.choosen
			disk.init(ChoosenDisk)
			state = State.game
			if PLAYMUSIC and not player.dead then Music[currentTrack]:play() end
			State.menu.pause = false
			started = true
			GameMode = "freeplay"
			GameTableName = GameMode
			GameTable = ModeTables[GameMode]
		elseif self.choosen == 4 then
			state = State.diskMenu
			state.load()
			GameMode = "time"
			GameTableName = "Time Survival"
			GameTable = ModeTables[GameMode]
		end
		print(GameMode or "null")
		if self.choosen == #Modes + 1 or self.choosen == 0 then
			state = State.menu
		end
	end
end

function State.modeMenu.draw()
	local self = State.modeMenu
	State.menu.draw()
	local h = MainFont:getHeight("W")
	drawRectangle("line", self.x, self.y - 8 + self.yof, self.maxX + 14, #self.menu.buttons * h * 4 + 12)
	for i = 1, #self.menu.buttons do
		local t = self.menu.buttons[i]
		if self.choosen == i then
			gr.rectangle("fill", self.x, self.y + i * self.yof, self.maxX + 10, self.yof - 4)
			gr.setBlendMode("subtractive")
		end
		gr.print(t, 10 + self.x, self.y + i * self.yof, 0, 4, 4)
		gr.setBlendMode("alpha")
	end
end

--[[

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

]]

State.diskMenu = {}
	State.diskMenu.x = 264
	State.diskMenu.y = State.menu.y
	State.diskMenu.yof = 28
	State.diskMenu.choosen = 0
	State.diskMenu.maxX = 230

function State.diskMenu.load()
	local h = MainFont:getHeight("W")
	State.diskMenu.y = State.menu.y
	local self = State.diskMenu
	self.menu = {buttons = {}}
	local p = 1
	for i = 1, #DiskStates do
		self.menu.buttons[i] = string.upper(DiskStates[i])
		p = i
	end
	p = p + 1
	local m = self.menu
	m.buttons[#DiskStates + 1] = "BACK"

	self.height = #self.menu.buttons * h * 4 + 12
end

function State.diskMenu.update(dt)
	local self = State.diskMenu
	self.choosen = 0
	add(self.y + self.height)
	add(my)
	if mx > self.x and mx < self.x + self.maxX + 14 then
		for i = 1, #self.menu.buttons do
			if my > self.y + i * self.yof and my < self.y + i * self.yof + self.yof then
				self.choosen = i
				break
			end
		end
	end
	if mousePressed("l") then
		SndClick:play()
		if self.choosen >= 1 and self.choosen <= #DiskStates then
			ChoosenDisk = DiskStates[self.choosen]
			DiskState = self.choosen
			disk.init(ChoosenDisk)
			state = State.game
			if PLAYMUSIC and not player.dead then Music[currentTrack]:play() end
			State.menu.pause = false
			started = true
			if GameMode == "classic" then
				GameTable = DiskTables[self.choosen]
				GameTableName = DiskStates[self.choosen]
			elseif GameMode == "time" then
				EnemyTable = {1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,5,5,5,4,2,2,6,6,6,7,7,7}
			end
		end
		if self.choosen == #DiskStates + 1 or self.choosen == 0 then
			state = State.modeMenu
		end
	end
end

function State.diskMenu.draw()
	local self = State.diskMenu
	State.menu.draw()
	local h = MainFont:getHeight("W")
	drawRectangle("line", self.x, self.y - 8 + self.yof, self.maxX + 14, #self.menu.buttons * h * 4 + 12)
	for i = 1, #self.menu.buttons do
		local t = self.menu.buttons[i]
		if self.choosen == i then
			gr.rectangle("fill", self.x, self.y + i * self.yof, self.maxX + 10, self.yof - 4)
			gr.setBlendMode("subtractive")
		end
		gr.print(t, 10 + self.x, self.y + i * self.yof, 0, 4, 4)
		gr.setBlendMode("alpha")
	end
end

--[[

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

]]

State.pause = {}
	State.pause.x = 0
	State.pause.y = 180
	State.pause.yof = 28
	State.pause.choosen = 0
	State.pause.maxX = 254

function State.pause.load()
	local h = MainFont:getHeight("W")
	local self = State.pause
	self.menu = {buttons = {"RESUME", "SETTINGS", "LOGIN", "STOP"}}
	local m = self.menu

	self.height = #self.menu.buttons * h * 4 + 12
	self.y = 300 - self.height / 2
end

function State.pause.update(dt)
	local self = State.pause
	self.choosen = 0
	add(self.y + self.height)
	add(my)
	if mx > self.x and mx < self.x + self.maxX + 14 then
		for i = 1, #self.menu.buttons do
			if my > self.y + i * self.yof and my < self.y + i * self.yof + self.yof then
				self.choosen = i
				break
			end
		end
	end
	if mousePressed("l") then
		SndClick:play()
		if self.choosen == 1 or self.choosen == 0 then
			state = State.game
			if PLAYMUSIC and not player.dead then Music[currentTrack]:play() end
			self.pause = false
			started = true
		elseif self.choosen == 2 then
			state = State.settings
			state.parent = State.pause
			state.load()
			state.y = State.pause.y
		elseif self.choosen == 3 then
			state = State.translate
			State.login.goto = State.pause
			state.load(State.pause, State.login, true)
		elseif self.choosen == 4 then
			if showSplashQuestion("Are you sure?", 400, 300, "center", "center") then
				state = State.translate
				state.load(State.pause, State.endScreen, true)
				self.pause = false
			end
		end
	end
	if keyPressed("escape") then
		state = State.game
		if PLAYMUSIC and not player.dead then Music[currentTrack]:play() end
		self.pause = false
		started = true
	end
end

function State.pause.draw()
	local self = State.pause
	State.game.draw()
	local h = MainFont:getHeight("W")
	drawRectangle("line", self.x, self.y - 8 + self.yof, self.maxX + 14, #self.menu.buttons * h * 4 + 12)
	for i = 1, #self.menu.buttons do
		local t = self.menu.buttons[i]
		if self.choosen == i then
			gr.rectangle("fill", self.x, self.y + i * self.yof, self.maxX + 10, self.yof - 4)
			gr.setBlendMode("subtractive")
		end
		gr.print(t, 10 + self.x, self.y + i * self.yof, 0, 4, 4)
		gr.setBlendMode("alpha")
	end
	local t = "ONE BIT ARENA"
	local w, h = MainFont:getWidth(t) * 10, MainFont:getHeight(t) * 10
	drawRectangle("line", 400 - w / 2 - 3, 0, w + 6, h + 6)
	printC(t, 400, h / 2 + 8, 10)
	if GJ.isLoggedIn then
		t = "You're logged in as \""..GJ.username.."\""
		w, h = MainFont:getWidth(t) * 4, MainFont:getHeight(t) * 4
		drawRectangle("line", 800 - w - 12, 600 - h - 12, w + 12, h + 12)
		printC(t, 800 - w / 2 - 6, 600 - h / 2 - 4, 4)
	end
end

--[[ HIGH

00  00  000000  00000000  00  00
00  00    00    00        00  00
000000    00    00  0000  000000
00  00    00    00    00  00  00
00  00  000000  00000000  00  00

]]

State.highMenu = {}
	State.highMenu.x = 264
	State.highMenu.y = State.menu.y
	State.highMenu.yof = 28
	State.highMenu.choosen = 0
	State.highMenu.maxX = 300

function State.highMenu.load()
	local h = MainFont:getHeight("W")
	State.highMenu.y = State.menu.y
	local self = State.highMenu
	self.menu = {buttons = {}, tables = {}}
	local p = 1
	for i = 1, #DiskStates do
		self.menu.buttons[p] = string.upper(DiskStates[i])
		self.menu.tables[p] = DiskTables[i]
		p = p + 1
	end
	for i = 2, #Modes do
		self.menu.buttons[p] = string.upper(ModeName[i])
		self.menu.tables[p] = ModeTables[Modes[i]]
		p = p + 1
	end
	--p = p + 1
	local m = self.menu
	self.menu.buttons[p] = "BACK"

	self.height = #self.menu.buttons * h * 4 + 12
end


function State.highMenu.update(dt)
	local self = State.highMenu
	self.choosen = 0
	add(self.y + self.height)
	add(my)
	if mx > self.x and mx < self.x + self.maxX + 14 then
		for i = 1, #self.menu.buttons do
			if my > self.y + i * self.yof and my < self.y + i * self.yof + self.yof then
				self.choosen = i
				break
			end
		end
	end
	if mousePressed("l") then
		SndClick:play()
		if self.choosen == #self.menu.buttons or self.choosen == 0 then
			state = State.menu
		else
			state = State.translate
			local timeScore = false
			if self.menu.buttons[self.choosen] == "TIME SURVIVAL" then
				timeScore = true
			end
			State.highscores.load(self.menu.tables[self.choosen], self.menu.buttons[self.choosen], timeScore, State.highMenu, false)
			state.load(State.highMenu, State.highscores)
		end
	end
end

function State.highMenu.draw()
	local self = State.highMenu
	State.menu.draw()
	local h = MainFont:getHeight("W")
	drawRectangle("line", self.x, self.y - 8 + self.yof, self.maxX + 14, #self.menu.buttons * h * 4 + 12)
	for i = 1, #self.menu.buttons do
		local t = self.menu.buttons[i]
		if self.choosen == i then
			gr.rectangle("fill", self.x, self.y + i * self.yof, self.maxX + 10, self.yof - 4)
			gr.setBlendMode("subtractive")
		end
		gr.print(t, 10 + self.x, self.y + i * self.yof, 0, 4, 4)
		gr.setBlendMode("alpha")
	end
end

--[[

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

]]

State.highscores = {}
self = State.highscores

function State.highscores.load(table, name, timeScore, goto, load)
	local self = State.highscores
	self.highOffset = 0
	self.tabName = name
	self.userHigh = 0
	self.userHighChoosen = false
	self.tempText = ""
	self.tempTextTime = 0
	i = 1
	self.disk = chosen
	self.connection = http.request("http://gamejolt.com")
	if self.connection then
		self.highscores = GJ.fetchScores(100, table)
	end
	self.TimeScore = timeScore or false

	self.gotoState = goto or State.menu
end

function State.highscores.exit()
	state = State.translate
	Name = ""
	EndScore = 0
	Score = 0
	Aggression = 0

	state.load(State.highscores, State.menu, false, true)
end

function State.highscores.exitFunc()
	state = State.translate
	state.load(State.highscores, State.highscores.gotoState, true, true)
end

function State.highscores.update(dt)
	local self = State.highscores

	if self.connection then

		if not self.userHighChoosen then
			if EndScore > 0 then
				for i, v in ipairs(self.highscores) do
					print(math.floor(v.sort)..":"..math.floor(EndScore))
					if (v.guest == Name or v.user == GJ.username) and math.floor(v.sort) == math.floor(EndScore) then
						self.userHigh = i
						break
					end
				end
				if self.userHigh == 0 then
					self.userHigh = -1
				end
			end
			self.userHighChoosen = true
			self.highOffet = self.userHigh
		end
		if self.userHighChoosen then
			if not ShowedTutor.highscores then
				ShowTutorText(TutorText.highscores)
				ShowedTutor.highscores = true
			end
		end
	end
	if keyPressed(" ") or keyPressed("return") or keyPressed("escape") then
		self.exitFunc()
	end

	self.tempTextTime = self.tempTextTime - dt
	if self.connection then
		if mousePressed("l") then
			local topY = 40
			local offset = 28
			local mc = math.floor((my - topY + 2) / offset) + self.highOffset
			if my > 40 then
				if mc <= #self.highscores then
					if mx > 220 and mx < 800 - MainFont:getWidth("00000000") * 4 then
						if self.highscores[mc].guest ~= "" then
							self.tempText = "It's a guest user!"
							self.tempText2 = "Try another..."
							self.tempTextTime = 2
						else
							openLink("http://gamejolt.com/profile/"..self.highscores[mc].user.."/"..self.highscores[mc].user_id)
						end
					end
				end
			end
		end
		if mousePressed("wu") then
			self.highOffset = self.highOffset - 1
			self.highOffset = math.max(0, self.highOffset)
		end
		if mousePressed("wd") then
			self.highOffset = self.highOffset + 1
			self.highOffset = math.min(#self.highscores - 19, self.highOffset)
		end
	end
	BackButton.update(State.highscores.exitFunc)
end

function State.highscores.draw()
	local self = State.highscores

	gr.rectangle("fill", 0, 0, 800, 600)
	gr.setBlendMode("subtractive")

	local topY = 40
	local offset = 28
	local mc = math.floor((my - topY + 2) / offset) + self.highOffset

	printC("TOP 100 FOR "..string.upper(self.tabName), 400, 16, 4)
	if self.connection then
		for i = 1, 20 do
			local j = i + self.highOffset
			if j <= #self.highscores and j >= 1 then
				gr.setBlendMode("subtractive")
				if self.userHigh == j or mc == j then
					gr.rectangle("fill", 0, topY + i * 28, 800, 24)
					gr.setBlendMode("alpha")
				end
				local name = ""
				if self.highscores[j].user ~= "" then name = self.highscores[j].user
				elseif self.highscores[j].guest ~= "" then name = self.highscores[j].guest end
				gr.print(StringZero(j, #tostring(#self.highscores))..": "..name, 10, topY + i * 28, 0, 4, 4)
				if self.highscores[j].guest ~= "" then
					gr.print("G", 450, topY + i * 28, 0, 4, 4)
				end
				local score = StringZero(self.highscores[j].sort, 8) .. " pts."
				if self.TimeScore then
					score = SecondsToTime(self.highscores[j].sort)
				end
				gr.print(score, 800 - MainFont:getWidth(score) * 4, topY + i * 28, 0, 4, 4)
			end
		end
	else
		printC("CONNECTION ERROR", 400, 275, 4)
		printC("SCORES NOT LOADED", 400, 325, 4)
	end
	if self.tempTextTime > 0 then
		drawRectangle("line", 200, 560 - 28, 400, 100)
		gr.setBlendMode("alpha")
		local t = self.tempText
		local w, h = MainFont:getWidth(t) * 4, MainFont:getHeight(t) * 4

		gr.print(self.tempText, 400 - w / 2, 560 - h / 2 - 8, 0, 4, 4)
		local t = self.tempText2
		local w, h = MainFont:getWidth(t) * 4, MainFont:getHeight(t) * 4
		gr.print(self.tempText2, 400 - w / 2, 584 - h / 2 - 4, 0, 4, 4)
	end
	gr.setBlendMode("alpha")

			BackButton.draw()
end

--[[ END

000000  00    00  0000
00      0000  00  00  00
0000    00  0000  00  00 
00      00    00  00  00
000000  00    00  0000

]]

State.endScreen = {}
self = State.endScreen

function State.endScreen.load()
	local self = State.endScreen
	started = false

	self.DISK = ChoosenDisk
	Name = ""
	Aggression = round(Kills / math.floor(Time), 2)
	EndScore = math.floor(Score * Aggression)
	if GameMode == "time" then
		EndScore = math.floor(Time)
	end
	EndScore1 = -EndScore
	EndScore2 = tostring(EndScore1)
	EndString1 = "Your game score:\nYour time:\nYour kills:\nYour Rage:\n\nTotal:"
	EndString2 = string.format("%d\n%d\n%d\n%f\n\n%d", Score, Time, Kills, Aggression, EndScore)
	self.table = GameTable
	if GameMode == "time" then
		EndString1 = "Your time:"
		EndString2 = SecondsToTime(Time)
	elseif Time == 0 or Score == 0 or CHEATING then
		state = State.highscores
		if GameMode == "time" then
			timeScore = true
		end
		state = State.highscores
		state.load(self.table, GameTableName, timeScore, State.menu, true)
		State.game.load()
	end
end

function State.endScreen.update(dt)
	local self = State.endScreen
	if not ShowedTutor.endScreen then
		ShowTutorText(TutorText.endScreen)
		ShowedTutor.endScreen = true
	end
	if not GJ.isLoggedIn then
		if keyPressed("backspace") then
			Name = string.sub(Name, 1, #Name - 1)
		end
		if keyPressed("return") then
			local timeScore = false
			local sendDescript = EndScore .. " pts"
			if GameMode == "time" then
				timeScore = true
				sendDescript = SecondsToTime(EndScore)
			end
			if not keyPressed("escape") then
				if Name ~= "" then
					local ok, signature = GJ.addScore(EndScore, sendDescript, self.table, Name)
					--[[if not ok then
						GJ.addScoreLocal(EndScore, sendDescript, self.table, signature, Name)
					end]]
				end
				CHEAT = false
			end
			if GameMode == "time" then
				timeScore = true
			end

			state = State.highscores			
			state.load(self.table, GameTableName, timeScore, State.menu, true)
			State.game.load()
		end
	else
		if keyPressed("y") or keyPressed("n") or keyPressed("return") or keyPressed("escape") then
			local timeScore = false
			local sendDescript = EndScore .. " pts"
			if GameMode == "time" then
				timeScore = true
				sendDescript = SecondsToTime(EndScore)
			end
			if not (keyPressed("escape") or keyPressed("n")) and (keyPressed("y") or keyPressed("return"))  then
			  	local ok, signature = GJ.addScore(EndScore, SecondsToTime(EndScore), self.table)
			  	if not ok then
			  		GJ.addScoreLocal(EndScore, sendDescript, self.table, signature)
			  	end
				CHEAT = false
			end
			state = State.highscores
			state.load(self.table, GameTableName, timeScore, State.menu, true)
			State.game.load()
		end
	end
end

function State.endScreen.draw()
	local self = State.endScreen
	State.game.draw()
	gr.rectangle("fill", 0, 0, 800, 600)
	gr.setBlendMode("subtractive")
	gr.print(EndString1, 100, 100, 0, 4, 4)
	gr.print(EndString2, 500, 100, 0, 4, 4)
	if not GJ.isLoggedIn then
		printC("Enter your name", 400, 480, 4)
		printC("[ "..Name.." ]", 400, 508, 4)
		printC("Or leave the name field\nempty to quit without submit", 400, 558, 4)
	else
		printC("Do you want to public your score?[Y/N]", 400, 500, 4)
	end
	gr.setBlendMode("alpha")
end

--[[
                   000
        00000000000000000 
000000000000000000000000000
        00000000000000000
                   000
]]

State.translate = {}

function State.translate.load(oldState, newState, load, up)
	local self = State.translate
	self.oldState = oldState
	self.newState = newState
	self.loading = load
	self.y = 0
	self.dir = 1
	if up then
		self.y = 600
		self.dir = -1
	end
end

function State.translate.update(dt)
	local self = State.translate
	self.y = self.y + 600 * dt * self.dir
	if self.y * self.dir > 302 * (self.dir + 1) then
		state = self.newState
		if self.loading then
			self.newState.load()
		end
	end
end

function State.translate.draw()
	local self = State.translate
	if self.dir == 1 then
		self.oldState.draw()
	else
		self.newState.draw()
	end
	gr.rectangle("fill", 0, 0, 800, math.floor(self.y / 4) * 4)
end

BackButton = {
	x = 0,
	y = 0,
	width = 44,
	height = 44,
	xoffset = 0,
	yoffset = 0,
	pointed = false,
	func = function() end
}

function BackButton.update(f)
	local b = BackButton
	b.pointed = false
	if mx > b.x - b.xoffset and my > b.y - b.yoffset and mx < b.x - b.xoffset + b.width and my < b.y - b.yoffset + b.height then
		b.pointed = true
	end
	if mousePressed("l") and b.pointed then
		SndClick:play()
		if f then f() end
	end
end

function BackButton.draw()
	local b = BackButton
	drawRectangle("line", b.x - b.xoffset, b.y - b.yoffset, b.width, b.height)
	if b.pointed then
		drawRectangle("fill", b.x + 8 - b.xoffset, b.y + 8 - b.yoffset, b.width - 16, b.height - 16)
		gr.setBlendMode("subtractive")
	end
	printC("<", b.x + b.width / 2 - b.xoffset, b.y + b.height / 2 - b.yoffset, 4)
	gr.setBlendMode("alpha")
end