Enemy = {}
for i = 1, 8 do
	Enemy[i] = {}
end

Enemy.img = {}
Enemy.img[1] = createImage({
	"---0000-",
	"--00----",
	"-00-0---",
	"000000--",
	"000000--",
	"-00-0---",
	"--00----",
	"---0000-"
})
Enemy.img[2] = createImage({
	"--------",
	"--00----",
	"-00-0---",
	"000000--",
	"000000--",
	"-00-0---",
	"--00----",
	"--------"
	})
Enemy.img[3] = createImage({
	"--000---",
	"-000-0--",
	"-000000-",
	"-000-0--",
	"--000---"
	})
Enemy.img[4] = createImage({
	"-0-0000-",
	"000-0000",
	"0000-000",
	"000-0000",
	"-0-0000-"
	})
Enemy.img[5] = createImage({
	"-000-00-",
	"00000-00",
	"000000-0",
	"00000-00",
	"-000-00-"
	})
Enemy.img[6] = createImage({
	"--0000--",
	"-000000-",
	"0000--00",
	"00000000",
	"00000000",
	"0000--00",
	"-000000-",
	"--0000--"
})
Enemy.img[7] = createImage({
	"000-0",
	"00000",
	"00000",
	"000-0",
	})
Enemy.img[8] = createImage({
	"0-----",
	"0000--",
	"000-00",
	"-00000",
	"-00000",
	"000-00",
	"0000--",
	"0-----"
	})
Enemy.img[9] = createImage({
	"000-000-",
	"000-0000",
	"000000-0",
	"-0000000",
	"-0000000",
	"000000-0",
	"000-0000",
	"000-000-",
	})
Enemy.img[10] = createImage({
	"--------",
	"--00--0-",
	"-00000-0",
	"-0000000",
	"-0000000",
	"-00000-0",
	"--00--0-",
	"--------",
	})
Enemy.img[12] = createImage({
	"-0--0---",
	"--00----",
	"-00-0---",
	"0000000-",
	"0000000-",
	"-00-0---",
	"--00----",
	"-0--0---"
	})
Enemy.img["explosion"] = createImage({
	"-----0000-----",
	"---00000000---",
	"--000----000--",
	"-00--------00-",
	"-00--------00-",
	"00----------00",
	"00----------00",
	"00----------00",
	"00----------00",
	"-00--------00-",
	"-00--------00-",
	"--000----000--",
	"---00000000---",
	"-----0000-----"
	
	})
--[[for i, v in ipairs(Enemy.img) do
	Enemy.img[i] = createImage(v)
end]]

function Enemy.place(t, x, y)
	local e = {}
	e.actor = "enemy"
	e.img = Enemy.img[t]
	e.x = x
	e.y = y
	e.hs = 0
	e.vs = 0
	e.dead = false
	e.deadTimer = 0
	e.update = Enemy.update
	e.update2 = Enemy[t].update
	e.draw = Enemy[t].draw
	e.deadAction = Enemy[t].deadAction
	e.visible = true
	table.insert(activities, e)
	e.t = t
	e.wait = false
	e.gotHit = 0
	e.seePlayer = false
	e.testTime = math.random()
	e.streaked = 0
	e.getScore = true
	e.solid = true
	e.viewDistance = 250
	Enemy[t].load(e)

	if GameMode == "hardcore" then
		e.hp = 1
	end
	if GameMode == "time" then
		e.spd = e.spd * 1.2
	end
	Quad.addActor(e)

	return e
end

function Enemy.update(act, dt)
	act.hs = 0
	act.vs = 0
	act.visible = true
	Quad.addActor(act)
	if act.hp > 0 then
		act.dirToPlayer = math.atan2(player.y - act.y, player.x - act.x)
		act.distToPlayer = math.sqrt((player.x - act.x)^2 + (player.y - act.y)^2)
		act.testTime = act.testTime - dt
		if act.testTime <= 0 then
			local actX, actY = math.floor(act.x / mapS), math.floor(act.y / mapS)
			local ox, oy = actX + 0.5, actY + 0.5
			act.seePlayer = true
			for i = 0, math.floor(math.sqrt((player.x-act.x)^2 + (player.y-act.y)^2) / 32), 0.1 do
				local dx, dy = ox + i * cos(act.dirToPlayer), oy + i * sin(act.dirToPlayer)
				local x, y, t = tilemap.get(dx, dy)
				if t == 2 or t == 3 then
					act.seePlayer = false
					break
				end
			end
			act.testTime = 1
		end
		if disk.throwed and disk.move and not disk.permanentHome then
			if act.hp > 0 and act.gotHit <= 0 and act.visible then
				if math.sqrt((disk.x - act.x)^2+(disk.y - act.y)^2) < act.radius + 8 then
					act.hp = act.hp - 1
					act.gotHit = 0.15
					disk.miss = false
					if act.hp == 0 then
						disk.strike = disk.strike + 1
						act.streaked = disk.strike - 1
						if act.getScore then
							Kills = Kills + 1
							Kills1 = Kills1 - 1
							Kills2 = tostring(tonumber(Kills2) - 1)
							if disk.action == "frisbee" then
								disk.comboTime = 3 - (math.floor(disk.combo / 10))
								disk.combo = disk.combo + 1
								act.streaked = disk.combo
							end
						end
					end
					SndHit:stop()
					SndHit:play()
					if disk.hitAction[disk.action] then
						disk.hitAction[disk.action]()
					end
					if disk.permanentHome then
					end
				end
			end
		end
		if EnemyAI then
			Enemy[act.t].update(act, dt)
			act.hs = act.spd * math.cos(act.angle)
			act.vs = act.spd * math.sin(act.angle)
		end
		if not act.wait and act.gotHit <= 0 then
			ActorMove(act, act.hs, act.vs, true)
		end
		--for _i, _v in ipairs(activities) do
		local actors = Quad.getQuadActor(act)
		table.foreachi(actors, function(_i, _v)
			if _v ~= act then
				local o = _v
				if o.actor ~= "a" then
					local dist = math.sqrt((act.x - o.x)^2 + (act.y - o.y)^2)
					local dir = math.atan2(o.y - act.y, o.x - act.x)
					local collisionDist = act.radius + _v.radius
					if o.solid == true and act.solid == true then
						if dist < collisionDist then
							dist = collisionDist - dist
							local ahs = dist / 2 * cos(dir + pi)
							local avs = dist / 2 * sin(dir + pi)
							local bhs = dist / 2 * cos(dir)
							local bvs = dist / 2 * sin(dir)
							ActorMove(act, ahs, avs)
							ActorMove(o, bhs, bvs)
						end
					end
				end
			end
		end)
	else
		if act.deadTimer == 0 and act.getScore and GameMode ~= "time" then
			scoreAdd(act.scoreAdd + act.strikeAdd * act.streaked, act.x, act.y)
			SndScore:stop()
			SndScore:play()
		elseif act.deadTimer == 0 then
			player.hp = player.hp + 5
		end
		act.deadTimer = act.deadTimer + dt
		act.solid = false
		act.visible = true
		if math.floor((act.deadTimer * 10) % 2) == 0 then
			act.visible = false
		end
		if act.deadTimer > 1 then
			table.insert(activitiesRemove, act)
			if act.deadAction then act.deadAction(act) end
			enemies = enemies - 1
			if act.getScore then
				if not ShowedTutor.kill then
					ShowTutorText(TutorText.kill)
					ShowedTutor.kill = true
				end
			end
		end
	end
	act.gotHit = act.gotHit - dt
	if act.gotHit > 0 then
		act.visible = false
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

Enemy[1].load = function(act)
	act.hp = 1 * DIFFICULT
	act.dir = 0
	act.angle = 0
	act.spd = 120
	act.dirSin = 0
	act.dirTime = 0
	act.maxDir = math.random() * pi / 4
	act.dirSinSpd = math.random() * 3 + 0.5
	act.scoreAdd = 25
	act.strikeAdd = 5
	act.radius = 13

	act.randomDir = math.random() * pi * 2
	act.randomDirT = math.random() * 2
	act.viewDistance = 350
end

Enemy[1].update = function(act, dt)	
	act.randomDirT = act.randomDirT - dt
	if act.randomDirT <= 0 then
		act.randomDirT = math.random() * 3
		act.randomDir = math.random() * pi * 2
	end
	act.dirTime = act.dirTime + dt * act.dirSinSpd
	act.dirSin = sin(act.dirTime)
	if act.distToPlayer < act.viewDistance then
		act.dir = math.atan2(player.y - act.y, player.x - act.x) + act.dirSin * act.maxDir
	else
		act.dir = act.randomDir + act.dirSin * act.maxDir
	end
	act.angle = slowDir(act.angle, act.dir, pi * 3 * dt)
	
	if math.sqrt((act.x - player.x)^2 + (act.y - player.y)^2) < act.radius + player.radius + 5 and not player.dead and player.solid then
		player.hited = true
		player.hp = player.hp - 10 * dt * DIFFICULT
	end
end

Enemy[1].draw = function(act)
	if act.visible then
		gr.draw(act.img, act.x, act.y, act.angle, sizeK, sizeK, 3, 4)
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

Enemy[2].load = function(act)
	act.hp = 2 * DIFFICULT
	act.dir = 0
	act.angle = 0
	act.spd = 80
	act.dirSin = 0
	act.dirTime = 0
	act.maxDir = math.random() * pi / 4
	act.dirSinSpd = math.random() * 3 + 0.5
	act.bullet = {
		x = 0,
		y = 0,
		shoot = false,
		dir = 0,
		spd = 300
	}
	act.shootAlarm = math.random() * 3
	act.waitAlarm = 0

	act.scoreAdd = 40
	act.strikeAdd = 10
	act.radius = 13


	act.randomDir = math.random() * pi * 2
	act.randomDirT = math.random() * 2
	act.viewDistance = 300
end

Enemy[2].update = function(act, dt)
	act.wait = true
	act.shootAlarm = act.shootAlarm - dt
	act.waitAlarm = act.waitAlarm - dt
	act.dirTime = act.dirTime + dt * act.dirSinSpd
	act.dirSin = sin(act.dirTime)
	act.dir = act.dirToPlayer + act.dirSin * act.maxDir
	act.angle = act.dirToPlayer
	
	if act.waitAlarm <= 0 and math.sqrt((player.x - act.x)^2 + (player.y - act.y)^2) > 100 then
		act.wait = false
	end
	
	if act.shootAlarm <= 0 and act.seePlayer then
		act.bullet = {
			x = act.x, y = act.y,
			shoot = true,
			dir = act.dirToPlayer,
			spd = 250,
			fly = 0
		}
		act.shootAlarm = math.random() * 2 + 1
		act.waitAlarm = 1
	end
	if act.bullet.shoot then
		local b = act.bullet
		b.fly = b.fly + b.spd * dt
		b.x = b.x + b.spd * math.cos(b.dir) * dt
		b.y = b.y + b.spd * math.sin(b.dir) * dt
		local x, y, t = tilemap.get(b.x, b.y, true)
		if math.sqrt((player.x - b.x)^2 + (player.y - b.y)^2) < 18 and player.solid then
			b.shoot = false
			player.hp = player.hp - 2 * DIFFICULT
		end
		if t == 2 or t == 3 then
			b.shoot = false
		end
		if b.fly > 800 then
			b.shoot = false
		end
	end
end

Enemy[2].draw = function(act)
	if act.visible then
		gr.draw(act.img, act.x, act.y, act.angle, sizeK, sizeK, 3, 4)
	end
	if act.bullet.shoot and act.hp > 0 then
		gr.rectangle("fill", act.bullet.x - 4, act.bullet.y - 4, 8, 8)
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

Enemy[3].load = function(act)
	act.angle = 0
	act.dir = 0
	act.dirSin = 0
	act.dirTime = 0
	act.maxDir = math.random() * pi / 4
	act.dirSinSpd = math.random() * 3 + 0.5
	act.state = 0
	act.spd = 100
	act.stateTimer = 0
	act.hp = 1 * DIFFICULT
	act.scoreAdd = 30
	act.strikeAdd = 8
	act.anim = false
	act.frame = 0
	act.radius = 12

	act.viewDistance = 500
	act.randomDir = math.random() * pi * 2
	act.randomDirT = math.random() * 2
end

Enemy[3].update = function(act, dt)
	act.randomDirT = act.randomDirT - dt
	if act.randomDirT <= 0 then
		act.randomDirT = math.random() * 3
		act.randomDir = math.random() * pi * 2
	end

	act.stateTimer = act.stateTimer - dt
	act.dirTime = act.dirTime + dt * act.dirSinSpd
	act.dirSin = sin(act.dirTime)
	if act.state == 0 then
		if act.distToPlayer < act.viewDistance then
			act.dir = math.atan2(player.y - act.y, player.x - act.x) + act.dirSin * act.maxDir
		else
			act.dir = act.randomDir + act.dirSin * act.maxDir
			act.stateTimer = 0.1
		end
		act.angle = slowDir(act.angle, act.dirToPlayer, pi * 3 * dt)
	end

	--[[if act.state == 2 then
		act.wait = true
	end]]
	if act.state == 1 then
		act.spd = math.min(act.spd + 700 * dt, 350)
		act.frame = (act.frame + dt * 8)
		if act.frame >= 2 then
			act.frame = act.frame - 2
		end
		if math.sqrt((act.x - player.x)^2 + (act.y - player.y)^2) < act.radius + player.radius + 5 and not player.dead and player.solid then
			if math.abs(angleDiff(act.angle, act.dirToPlayer)) < pi / 5 then
				player.hp = player.hp - 8 * DIFFICULT
				act.state = 2
				act.stateTimer = 1.2
				act.wait = true
				act.anim = false
				act.spd = 100
			end
		end
	end

	if act.stateTimer < 0 then
		if act.state == 0 and act.seePlayer then
			act.dir = act.dirToPlayer
			--act.spd = 400
			act.state = 1
			act.stateTimer = 2
			act.anim = true
		elseif act.state == 1 then
			act.state = 2
			act.stateTimer = 1.2
			act.wait = true
			act.anim = false
			act.spd = 100
		elseif act.state == 2 then
			act.stateTimer = 3
			act.state = 0
			act.wait = false
		end
	end
end

Enemy[3].draw = function(act)
	if act.visible then
		local frame = 3
		if act.anim then
			frame = 4 + math.floor(act.frame)
		end
		gr.draw(Enemy.img[frame], act.x, act.y, act.angle, 4, 4, 3.5, 3.5)
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

Enemy[4].load = function(act)
	act.hp = 6 * DIFFICULT
	act.dir = 0
	act.angle = 0
	act.spd = 70
	act.dirSin = 0
	act.dirTime = 0
	act.maxDir = math.random() * pi / 4
	act.dirSinSpd = math.random() * 3 + 0.5
	act.scoreAdd = 80
	act.strikeAdd = 20
	act.radius = 15
	act.img = Enemy.img[6]

	act.viewDistance = 350
	act.randomDir = math.random() * pi * 2
	act.randomDirT = math.random() * 2
end

Enemy[4].update = function(act, dt)
	act.randomDirT = act.randomDirT - dt
	if act.randomDirT <= 0 then
		act.randomDirT = math.random() * 3
		act.randomDir = math.random() * pi * 2
	end

	act.dirTime = act.dirTime + dt * act.dirSinSpd
	act.dirSin = sin(act.dirTime)
	if act.distToPlayer < act.viewDistance then
		act.dir = math.atan2(player.y - act.y, player.x - act.x) + act.dirSin * act.maxDir
	else
		act.dir = act.randomDir + act.dirSin * act.maxDir
	end
	act.angle = slowDir(act.angle, act.dir, pi * 3 * dt)
	act.spd = 70
	
	if math.sqrt((act.x - player.x)^2 + (act.y - player.y)^2) < act.radius + player.radius + 5 and not player.dead and player.solid then
		act.spd = 140
		player.hited = true
		player.hp = player.hp - 20 * dt * DIFFICULT
	end
end

Enemy[4].draw = function(act)
	if act.visible then
		gr.draw(act.img, act.x, act.y, act.angle, sizeK, sizeK, 3, 4)
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

Enemy[5].load = function(act)
	act.hp = 1 * DIFFICULT
	act.dir = 0
	act.angle = 0
	act.spd = 150
	act.dirSin = 0
	act.dirTime = 0
	act.maxDir = math.random() * pi / 4
	act.dirSinSpd = math.random() * 3 + 0.5
	act.scoreAdd = 30
	act.strikeAdd = 5
	act.radius = 8
	act.deadTimer = 0.5
	act.img = Enemy.img[7]

	act.viewDistance = 300
	act.randomDir = math.random() * pi * 2
	act.randomDirT = math.random() * 2
end

Enemy[5].update = function(act, dt)

	act.randomDirT = act.randomDirT - dt
	if act.randomDirT <= 0 then
		act.randomDirT = math.random() * 3
		act.randomDir = math.random() * pi * 2
	end

	act.dirTime = act.dirTime + dt * act.dirSinSpd
	act.dirSin = sin(act.dirTime)
	if act.distToPlayer < act.viewDistance then
		act.dir = math.atan2(player.y - act.y, player.x - act.x) + act.dirSin * act.maxDir
	else
		act.dir = act.randomDir + act.dirSin * act.maxDir
	end
	act.angle = slowDir(act.angle, act.dir, pi * 3 * dt)
	act.getScore = true
	if math.sqrt((act.x - player.x)^2 + (act.y - player.y)^2) < act.radius + player.radius + 4 and not player.dead and player.solid then
		act.hp = 0 * DIFFICULT
		act.dead = true
		act.getScore = false
	end
end

Enemy[5].draw = function(act)
	if act.visible then
		gr.draw(act.img, act.x, act.y, act.angle, sizeK, sizeK, 3, 2.5)
	end
end

Enemy[5].deadAction = function(act)
	Explosion(act.x, act.y)
	if math.sqrt((act.x - player.x)^2 + (act.y - player.y)^2) < act.radius + player.radius + 40 and not player.dead then
		player.hp = player.hp - 25 * DIFFICULT
	end
	for i, v in ipairs(activities) do
		if v ~= act then
			if v.actor == "enemy" then
				if math.sqrt((v.x - act.x)^2+(v.y - act.y)^2) < act.radius + v.radius + 40 and not v.dead then
					v.gotHit = 0.1
					v.hp = v.hp - 2
					if v.hp <= 0 then
						v.getScore = act.getScore
						v.streaked = act.streaked + 1
					end
				end
			end
		end
	end
	local expl = {}
	SndExp:stop()
	SndExp:play()
end

--#####################################################################################################################################
--#####################################################################################################################################

Enemy[6].load = function(act)
	act.hp = 3 * DIFFICULT
	act.dir = 0
	act.angle = 0
	act.visibleTimer = 0
	act.spd = 100
	act.dirSin = 0
	act.dirTime = 0
	act.maxDir = math.random() * pi / 4
	act.dirSinSpd = math.random() * 3 + 0.5
	act.scoreAdd = 50
	act.strikeAdd = 20
	act.radius = 13
	act.img = Enemy.img[8]

	act.viewDistance = 200
	act.randomDir = math.random() * pi * 2
	act.randomDirT = math.random() * 2
end

Enemy[6].update = function(act, dt)

	act.randomDirT = act.randomDirT - dt
	if act.randomDirT <= 0 then
		act.randomDirT = math.random() * 3
		act.randomDir = math.random() * pi * 2
	end

	if act.distToPlayer > 200 then
		act.visibleTimer = math.min(act.visibleTimer + dt, 0.5)
	else
		act.visibleTimer = math.max(0, act.visibleTimer - dt)
	end

	act.visible = true
	if math.floor((act.visibleTimer * 10) % 2) == 0 and act.visibleTimer > 0 or act.visibleTimer == 0.5 then
		act.visible = false
	end
	act.dirTime = act.dirTime + dt * act.dirSinSpd
	act.dirSin = sin(act.dirTime)
	if act.distToPlayer < act.viewDistance then
		act.dir = math.atan2(player.y - act.y, player.x - act.x) + act.dirSin * act.maxDir
	else
		act.dir = act.randomDir + act.dirSin * act.maxDir
	end
	act.angle = slowDir(act.angle, act.dir, pi * 3 * dt)
	
	if math.sqrt((act.x - player.x)^2 + (act.y - player.y)^2) < act.radius + player.radius + 5 and not player.dead and player.solid then
		player.hited = true
		player.hp = player.hp - 20 * dt * DIFFICULT
	end
end

Enemy[6].draw = function(act)
	if act.visible then
		gr.draw(act.img, act.x, act.y, act.angle, sizeK, sizeK, 3, 4)
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

Enemy[7].load = function(act)
	act.hp = 5 * DIFFICULT
	act.dir = 0
	act.angle = 0
	act.spd = 100
	act.dirSin = 0
	act.dirTime = 0
	act.maxDir = math.random() * pi / 6
	act.dirSinSpd = math.random() * 3 + 0.5
	act.scoreAdd = 25
	act.strikeAdd = 5
	act.radius = 13
	act.img = Enemy.img[9]

	act.viewDistance = 350
	act.randomDir = math.random() * pi * 2
	act.randomDirT = math.random() * 2
end

Enemy[7].update = function(act, dt)
	act.randomDirT = act.randomDirT - dt
	if act.randomDirT <= 0 then
		act.randomDirT = math.random() * 3
		act.randomDir = math.random() * pi * 2
	end
	act.dirTime = act.dirTime + dt * act.dirSinSpd
	act.dirSin = sin(act.dirTime)
	if act.distToPlayer < act.viewDistance then
		act.dir = math.atan2(player.y - act.y, player.x - act.x) + act.dirSin * act.maxDir
	else
		act.dir = act.randomDir + act.dirSin * act.maxDir
	end
	act.angle = slowDir(act.angle, act.dir, pi * 3 * dt)
	if act.hp < 3 then
		act.img = Enemy.img[10]
		act.spd = 190
	end
	
	if math.sqrt((act.x - player.x)^2 + (act.y - player.y)^2) < act.radius + player.radius + 5 and not player.dead and player.solid then
		player.hited = true
		player.hp = player.hp - 30 * dt * DIFFICULT
	end
end

Enemy[7].draw = function(act)
	if act.visible then
		gr.draw(act.img, act.x, act.y, act.angle, sizeK, sizeK, 3, 4)
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

Enemy[8].load = function(act)
	act.hp = 5 * DIFFICULT
	act.dir = 0
	act.angle = 0
	act.scoreAdd = 50
	act.strikeAdd = 15
	act.radius = 13
	act.teleportTimer = 5
	act.img = Enemy.img[12]
end

Enemy[8].update = function(act, dt)
	act.dir = math.atan2(player.y - act.y, player.x - act.x)
	act.angle = slowDir(act.angle, act.dir, pi * 3 * dt)
	
end

Enemy[8].draw = function(act)
	if act.visible then
		gr.draw(act.img, act.x, act.y, act.angle, sizeK, sizeK, 3, 4)
	end
end

--#####################################################################################################################################
--#####################################################################################################################################

Explosion = function(x, y)
	local a = {}
	a.x = x
	a.y = y
	a.time = 0
	a.solid = false
	a.radius = 30
	a.update = ExplosionUpdate
	a.draw = ExplosionDraw
	a.dead = false
	a.type = "effect"
	a.size = 4
	table.insert(activities, a)
	return a
end

function ExplosionUpdate(act, dt)
	act.time = act.time + dt
	Quad.addActor(act)
	if act.time >= 0.7 then
		table.insert(activitiesRemove, act)
	end
	act.visible = not act.visible
end

function ExplosionDraw(act)
	if act.visible then
		gr.draw(Enemy.img["explosion"], act.x, act.y, 0, act.size, act.size, 7, 7)
	end
end
