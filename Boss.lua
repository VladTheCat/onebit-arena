Boss = {}

for i = 1, 3 do
	Boss[i] = {}
end

Boss.Img = {}

Boss.Img[1] = createImage({
		"----------------",
		"----00----------",
		"-00--0000000----",
		"--0000000000000-",
		"-00000000000000-",
		"000000-00-00-00-",
		"0000-0--0--0--0-",
		"000000----------",
		"000000----------",
		"0000-0--0--0--0-",
		"000000-00-00-00-",
		"-00000000000000-",
		"--0000000000000-",
		"-00--0000000----",
		"----00----------",
		"----------------"}
	)
Boss.Img[2] = createImage({
		"----------------",
		"-0--0-----------",
		"--0--0----------",
		"--0000000000----",
		"-00000000000000-",
		"000000000000000-",
		"0000-0-00-00-00-",
		"000000--0--0--0-",
		"000000--0--0--0-",
		"0000-0-00-00-00-",
		"000000000000000-",
		"-00000000000000-",
		"--0000000000----",
		"--0--0----------",
		"-0--0-----------",
		"----------------"}
	)
Boss.Img[3] = createImage({
		"--------",
		"--000---",
		"0000-00-",
		"00000000",
		"00000000",
		"0000-00-",
		"--000---",
		"--------"
	})
Boss.Img[4] = createImage({
		"--0--0--",
		"--0000--",
		"-00-000-",
		"0000-000",
		"0000-000",
		"-00-000-",
		"--0000--",
		"--0--0--"
	})
Boss.Img[5] = createImage({
		"--------",
		"-----0--",
		"---0000-",
		"00000000",
		"00000000",
		"---0000-",
		"-----0--",
		"--------"
	})
Boss.Img[6] = createImage({
		"----0000--000---",
		"---0000--000----",
		"--00000000------",
		"-00000000000----",
		"-00000000--00---",
		"-00000000--000--",
		"--0000000000000-",
		"--0000000000000-",
		"--0000000000000-",
		"--0000000000000-",
		"-00000000--000--",
		"-00000000--00---",
		"-00000000000----",
		"--00000000------",
		"---0000--000----",
		"----0000--000---"})

Boss.Img[7] = createImage({
		"-0000------0000-",
		"---000----000---",
		"--000000000-----",
		"-00000000000----",
		"-00000000--00---",
		"-00000000--000--",
		"--0000000000000-",
		"--0000000000000-",
		"--0000000000000-",
		"--0000000000000-",
		"-00000000--000--",
		"-00000000--00---",
		"-00000000000----",
		"--0000000000----",
		"---000----000---",
		"-0000------0000-"})

Boss.Img[8] = createImage({
		"------",
		"000000",
		"000-00",
		"000-00",
		"000000",
		"------",
	})


Boss[1].place = function(x, y)
	local act = {}
	act.actor = "boss"
	act.img = Boss.Img[1]
	act.frame = 0
	act.x = x
	act.y = y
	act.hs = 0
	act.vs = 0
	act.angle = 0
	act.dead = false
	act.update = Boss[1].update
	act.draw = Boss[1].draw
	act.visible = true
	table.insert(activities, act)
	act.gotHit = 0
	act.hp = 50 * DIFFICULT
	act.maxHP = 50 * DIFFICULT

	act.gotoX, act.gotoY = 0, 0
	act.act = 0
	act.actT = 5
	act.dirFromPlayer = math.random() * pi * 2
	act.circleDir = 1
	act.circleDirT = math.random(3, 7)
	act.distFromPlayer = 250
	act.distSin = 0
	act.radius = 30

	act.shootAlr = 0.5

	act.deadTimer = 0

	if PLAYMUSIC then
		Music[currentTrack]:stop()
		currentTrack = math.random(8, 12)
		Music[currentTrack]:play()
	end

	return act
end

Boss[1].update = function(act, dt)
	if act.hp > 0 then
		act.actT = act.actT - dt

		if act.actT <= 0 then
			if act.act == 0 then
				act.act = 1
				act.gotoX = player.x + 300 * cos(act.dirToPlayer) + player.hs / 6
				act.gotoY = player.y + 300 * sin(act.dirToPlayer) + player.vs / 6
				act.dirT = 3
				act.actT = 9
			elseif act.act == 1 then
				act.act = 0
				act.actT = 5
			end
		end

		if disk.throwed and disk.move and not disk.permanentHome then
			if act.hp > 0 and act.visible then
				if math.sqrt((disk.x - act.x)^2+(disk.y - act.y)^2) < act.radius + 8 then
					act.hp = act.hp - 1
					act.gotHit = 0.10
					SndHit:stop()
					SndHit:play()
					disk.canCatch = true
					disk.permanentHome = true
					if disk.hitAction[disk.action] then
						disk.hitAction[disk.action]()
					end
					if disk.permanentHome then
					end
				end
			end
		end

		act.frame = act.frame + dt * 3
		if act.frame > 2 then
			act.frame = act.frame - 2
		end
		act.dirToPlayer = math.atan2(player.y - act.y + player.vs / 7, player.x - act.x + player.hs / 7)
		act.distToPlayer = math.sqrt((player.x - act.x)^2 + (player.y - act.y)^2)
		act.angle = slowDir(act.angle, act.dirToPlayer, pi * dt * 1.5)

		if act.act == 0 then
			act.shootAlr = act.shootAlr - dt
			act.dirFromPlayer = act.dirFromPlayer + pi / 2 * act.circleDir * dt
			act.distSin = act.distSin + pi / 2 * dt
		    act.distSin = act.distSin % (pi * 2)
			act.circleDirT = act.circleDirT - dt
			if act.circleDirT < 0 then
				act.circleDir = choose(-1, 1)
				act.circleDirT = math.random(3, 7)
			end
			act.distFromPlayer = 250 + 100 * sin(act.distSin)
			act.gotoX = player.x + act.distFromPlayer * cos(act.dirFromPlayer)
			act.gotoY = player.y + act.distFromPlayer * sin(act.dirFromPlayer)

			if act.shootAlr <= 0 then
				Boss[1].shoot(act)
				act.shootAlr = 0.5
			end
		elseif act.act == 1 then
			act.dirT = act.dirT - dt
			if act.dirT <= 0 then
				act.dirT = 3
				
				act.gotoX = player.x + 400 * cos(act.dirToPlayer) + player.hs / 2.5
				act.gotoY = player.y + 400 * sin(act.dirToPlayer) + player.vs / 2.5
			end
		end
		local gotoDist = math.sqrt((act.gotoX - act.x)^2 + (act.gotoY - act.y)^2)
		local gotoDir = math.atan2(act.gotoY - act.y, act.gotoX - act.x)

		if act.distToPlayer < act.radius + player.radius + 5 and not player.dead and player.solid then
			player.hited = true
			player.hp = player.hp - 40 * dt
		end

		act.x = act.x + (act.gotoX - act.x) * dt
		act.y = act.y + (act.gotoY - act.y) * dt
		act.img = Boss.Img[math.floor(act.frame) + 1]

		act.gotHit = act.gotHit - dt
		act.visible = true
		if act.gotHit > 0 then
			act.visible = false
		end
	else
		act.deadTimer = act.deadTimer + dt
		act.solid = false
		act.visible = true
		if math.floor((act.deadTimer * 10) % 2) == 0 then
			act.visible = false
		end
		if act.deadTimer > 3 then
			table.insert(activitiesRemove, act)
			if act.deadAction then act.deadAction(act) end
			scoreAdd(10000 * DIFFICULT, act.x, act.y)
			SndScore:stop()
			SndScore:play()
			local e = Explosion(act.x, act.y)
			e.size = 8
			EnemyTable = {}
			DIFFICULT = DIFFICULT + 1
			currentWave = 1
			boss = 0
			Music[currentTrack]:stop()
			for i, v in ipairs(activities) do
				if v.actor == "enemy" then
					v.hp = 0
					v.getScore = false
				end
			end
		end
	end
end

Boss[1].draw = function(act)
	if act.visible then
		gr.draw(act.img, act.x, act.y, act.angle, sizeK, sizeK, 4, 8)
	end
end

Boss[1].bullUpdate = function(act, dt)
	act.x = act.x + 300 * cos(act.dir) * dt
	act.y = act.y + 300 * sin(act.dir) * dt
	act.life = act.life - dt
	if act.life <= 0 then
		table.insert(activitiesRemove, act)
		return false
	end
	if math.sqrt((player.x - act.x)^2 + (player.y - act.y)^2) < player.radius then
		player.hp = player.hp - 4
		table.insert(activitiesRemove, act)
	end

	
end

Boss[1].bullDraw = function(act)
	gr.rectangle("fill", act.x - 4, act.y - 4, 8, 8)
end

Boss[1].shoot = function(act)
	for i = -1, 1 do
		local a = {}
		a.actor = "a"
		a.x = act.x
		a.y = act.y
		a.dir = act.dirToPlayer + pi / 6 * i
		a.life = 4
		a.update = Boss[1].bullUpdate
		a.draw = Boss[1].bullDraw

		table.insert(activities, a)
	end
end

--[[
############################################################################################################
############################################################################################################
############################################################################################################
]]

Boss[2].place = function(x, y)
	local act = {}
	act.segments = 40
	act.actor = "boss"
	act.img = Boss.Img[3]
	act.frame = 0
	act.x = x
	act.y = y
	act.hs = 0
	act.vs = 0
	act.angle = 0

	act.dirSin = 0
	act.dirTime = 0
	act.maxDir = pi / 70
	act.dirSinSpd = 8

	act.dead = false
	act.update = Boss[2].update
	act.draw = Boss[2].draw
	act.visible = true
	table.insert(activities, act)
	act.gotHit = 0
	act.hp = 40 + 30 * DIFFICULT
	act.maxHP = 40 + 30 * DIFFICULT
	act.spd = 300

	act.gotoX, act.gotoY = 0, 0
	act.act = 0
	act.actT = 5
	act.radius = 30

	act.shootAlr = 0.5
	act.deadTimer = 0

	act.maxSpd = 800
	act.minSpd = 600
	act.timer = 10

	act.prevs = {}

	act[1] = {
		angle = 0,
		x = act.x,
		y = act.y,
		img = Boss.Img[3]
	}
	for i = 2, act.segments-1 do
		act[i] = {
			angle = 0,
			x = act.x - 30 * i,
			y = act.y,
			img = Boss.Img[4]
		}
	end
	act[act.segments] = {
		angle = 0,
		x = act.x - 330,
		y = act.y,
		img = Boss.Img[5]
	}
	act.addTime = 0

	if PLAYMUSIC then
		Music[currentTrack]:stop()
		currentTrack = math.random(8, 12)
		Music[currentTrack]:play()
	end

	return act
end

Boss[2].update = function(act, dt)
	act.visible = true
	if act.hp > 0 then
		act.dirTime = act.dirTime + dt * act.dirSinSpd
		act.dirSin = sin(act.dirTime)
		act.timer = act.timer - dt
		if act.timer < 0 then
			act.maxSpd = 700
			act.minSpd = 400
			act.timer = math.random(3, 8)
		end
		if not player.dead then
			act.dirToPlayer = math.atan2(player.y - act.y + player.vs / 7, player.x - act.x + player.hs / 7)
			act.angle = slowDir(act.angle, act.dirToPlayer, pi / 2 * dt * (600 / act.spd)) + act.dirSin * act.maxDir
			if math.abs(angleDiff(act.dirToPlayer, act.angle)) > pi / 5 then
				act.spd = math.max(act.minSpd, act.spd - 100 * dt)
			else
				act.spd = math.min(act.maxSpd, act.spd + 500 * dt)
			end
		else
			act.angle = slowDir(act.angle, 0.7*pi, pi / 2 * dt)
			act.spd = math.min(1000, act.spd + 300 * dt)
		end
		act.x = act.x + act.spd * cos(act.angle) * dt
		act.y = act.y + act.spd * sin(act.angle) * dt
		act[1].x = act.x
		act[1].y = act.y
		act[1].angle = act.angle
		for i = 1, act.segments do
			if i > 1 then
				local a = act[i - 1]
				local b = act[i]

				local dir = math.atan2(b.y - a.y, b.x - a.x)
				b.x = a.x + 28 * cos(dir)
				b.y = a.y + 28 * sin(dir)
				b.angle = dir + pi
			end
			local a = act[i]
			if disk.throwed and disk.move and not disk.permanentHome and act.gotHit <= 0 then
				if act.hp > 0 and act.visible then
					if math.sqrt((disk.x - a.x)^2+(disk.y - a.y)^2) < 16 + 8 then
						act.hp = act.hp - 1
						act.gotHit = 0.10
						SndHit:stop()
						SndHit:play()
						--disk.canCatch = true
						--disk.permanentHome = true
						if disk.hitAction[disk.action] then
							disk.hitAction[disk.action]()
						end
					end
				end
			end
			if math.sqrt((player.x - a.x)^2 + (player.y - a.y)^2) < player.radius + 16 then
				player.hp = player.hp - 15 * dt
			end
		end
	else
		act.deadTimer = act.deadTimer + dt
		act.solid = false
		act.visible = true
		if math.floor((act.deadTimer * 10) % 2) == 0 then
			act.visible = false
		end
		if act.deadTimer > 3 then
			table.insert(activitiesRemove, act)
			if act.deadAction then act.deadAction(act) end
			scoreAdd(10000 * DIFFICULT, act.x, act.y)
			SndScore:stop()
			SndScore:play()
			for i = 1, act.segments do
				Explosion(act[i].x, act[i].y)
				SndExp:play()
			end
			EnemyTable = {}
			DIFFICULT = DIFFICULT + 1
			currentWave = 1
			boss = 0
			Music[currentTrack]:stop()
			for i, v in ipairs(activities) do
				if v.actor == "enemy" then
					v.hp = 0
					v.getScore = false
				end
			end
		end
	end
	act.gotHit = act.gotHit - dt
	if act.gotHit > 0 then
		act.visible = false
	end
end

Boss[2].draw = function(act)
	if act.visible then
		for i = 1, act.segments do
			gr.draw(act[i].img, act[i].x, act[i].y, act[i].angle, 4, 4, 4, 4)
		end
	end
end

--[[
############################################################################################################
############################################################################################################
############################################################################################################
]]

Boss[3].place = function(x, y)
	local act = {}
	act.actor = "boss"
	act.img = Boss.Img[6]
	act.frame = 0
	act.x = x
	act.y = y
	act.hs = 0
	act.vs = 0
	act.angle = 0
	act.dead = false
	act.update = Boss[3].update
	act.draw = Boss[3].draw
	act.visible = true
	table.insert(activities, act)
	act.gotHit = 0
	act.hp = 50 * DIFFICULT
	act.maxHP = 50 * DIFFICULT
	act.z = 50
	act.zs = 0
	act.visibleTime = 0

	act.gotoX, act.gotoY = 0, 0
	act.act = 0
	act.actT = 5
	act.radius = 30

	act.catchTarget = nil
	act.distance = 0

	act.deadTimer = 0

	if PLAYMUSIC then
		Music[currentTrack]:stop()
		currentTrack = math.random(8, 12)
		Music[currentTrack]:play()
	end
	act.time = 0
	act.tries = 0

	return act
end

Boss[3].update = function(act, dt)
	if act.hp > 0 then

		act.dirToPlayer = math.atan2(player.y - act.y + player.vs / 5, player.x - act.x + player.hs / 5)
		act.distToPlayer = math.sqrt((player.x - act.x)^2 + (player.y - act.y)^2)
		act.time = act.time - dt

		if act.time <= 0 then
			local actX, actY = math.floor(act.x / mapS), math.floor(act.y / mapS)
			local ox, oy = actX + 0.5, actY + 0.5
			act.seePlayer = true
			act.time = act.time - dt
			for i = 0, math.floor(math.sqrt((player.x-act.x)^2 + (player.y-act.y)^2) / 32), 0.1 do
				local dx, dy = ox + i * cos(act.dirToPlayer), oy + i * sin(act.dirToPlayer)
				local x, y, t = tilemap.get(dx, dy)
				if t == 2 or t == 3 then
					act.seePlayer = false
					break
				end
			end
			act.time = 0.1
		end

		if act.act == 0 then   --- FLYING
			act.zs = act.zs - 300 * dt
			act.z = act.z + sign(act.zs) * 10 * dt
			if act.z > 7 then
				act.x = player.x
				act.y = player.y
			end
			if act.z < 0 then
				act.z = 0
				local e = Explosion(act.x, act.y)
				for i = 0, 359, 15 do
					Boss[3].shoot(act, math.rad(i))
				end
				if math.sqrt((player.x-act.x)^2 + (player.y-act.y)^2) < 70 then
					player.hp = player.hp - 25
				end
				e.size = 8
				act.act = 1
				act.actT = 2
				act.tries = 0
			end
		elseif act.act == 1 then	--- AFTER FALL
			act.actT = act.actT - dt
			if act.actT < 0 then
				act.act = 2
				act.actT = 3
			end
		elseif act.act == 2 then	--- TARGETING
			act.actT = act.actT - dt
			if act.tries == 5 then
				act.act = 0
				act.zs = 300
			end
			act.angle = slowDir(act.angle, act.dirToPlayer, pi * dt * 1.5)
			if act.actT <= 0 then
				if act.seePlayer then
					act.act = 3
					act.tries = act.tries + 1
				end
			end
		elseif act.act == 3 then	--- SHOOT
			act.distance = act.distance + 500 * dt
			local dx, dy = act.x + act.distance * cos(act.angle), act.y + act.distance * sin(act.angle)
			for index, object in ipairs(activities) do
				if object.actor == "enemy" or object.actor == "player" then
					if math.sqrt((object.x-dx)^2 + (object.y-dy)^2) < 30 then
						act.catchTarget = object
						act.act = 5
						act.actT = 2
					end
				end
			end
			--[[local dist = math.sqrt((player.x - dx)^2 + (player.y - dy)^2)
			add("DIST: "..dist)
			if dist < 30 then
				act.act = 5
				act.actT = 2
			end]]
			local x, y, t = tilemap.get(act.x+act.distance*cos(act.angle), act.y+act.distance*sin(act.angle), true)
			if (t == 2 or t == 3) then
				act.act = 4
				act.actT = 5
				act.hits = 0
			end
			if act.distance > 500 then
				act.act = 4
			end
		elseif act.act == 4 then	--- AFTER SHOOT
			act.distance = act.distance - 700 * dt
			if act.distance < 0 then
				act.distance = 0
				act.act = 2
				act.actT = 0.5
			end
		elseif act.act == 5 then	--- CATHED OBJECT
			act.actT = act.actT - dt
			act.distance = act.distance - 500 * dt
			act.catchTarget.x = act.x + act.distance * cos(act.angle)
			act.catchTarget.y = act.y + act.distance * sin(act.angle)
			if act.distance < 30 then
				act.distance = 30
				if act.catchTarget.actor == "enemy" then
					act.catchTarget.hp = act.catchTarget.hp - 2 * dt
					act.catchTarget.getScore = false
				end
			end
			if act.actT <= 0 then
				act.act = 2
				act.actT = 1.5
				act.distance = 0
			end
		elseif act.act == 6 then	--- AFTER SHOOT 2
			act.actT = act.actT - dt
			if act.hits > 5 or act.actT <= 0 then
				act.act = 4
			end
		end

		act.visible = true
		act.img = Boss.Img[6]
		if act.z > 0 then
			act.img = Boss.Img[7]
			if act.z > 7 or (act.z > 0 and act.z < 7 and act.z % 0.5 < 0.25) then
				act.visible = false
			end
		end

		if act.distToPlayer < act.radius + player.radius and act.z == 0 then
			player.hp = player.hp - 15 * dt
		end

		if disk.throwed and disk.move and not disk.permanentHome then
			if act.hp > 0 and act.visible then
				if math.sqrt((disk.x - act.x)^2+(disk.y - act.y)^2) < act.radius + 8 then
					act.hp = act.hp - 1
					act.gotHit = 0.10
					SndHit:stop()
					SndHit:play()
					disk.canCatch = true
					disk.permanentHome = true
					if disk.hitAction[disk.action] then
						disk.hitAction[disk.action]()
					end
					if disk.permanentHome then
					end
				end
			end
		end
	else
		act.deadTimer = act.deadTimer + dt
		act.solid = false
		act.visible = true
		if math.floor((act.deadTimer * 10) % 2) == 0 then
			act.visible = false
		end
		if act.deadTimer > 3 then
			table.insert(activitiesRemove, act)
			if act.deadAction then act.deadAction(act) end
			scoreAdd(10000 * DIFFICULT, act.x, act.y)
			SndScore:stop()
			SndScore:play()
			Explosion(act.x, act.y)
			SndExp:play()
			EnemyTable = {}
			DIFFICULT = DIFFICULT + 1
			currentWave = 1
			boss = 0
			Music[currentTrack]:stop()
			for i, v in ipairs(activities) do
				if v.actor == "enemy" then
					v.hp = 0
					v.getScore = false
				end
			end
		end
	end
	act.gotHit = act.gotHit - dt
	if act.gotHit > 0 then
		act.visible = false
	end
end

Boss[3].draw = function(act)
	if act.visible then
		gr.draw(act.img, act.x, act.y, act.angle, 4, 4, 8, 8)
		for i = 1, act.distance / 24 do
			gr.draw(Boss.Img[8], act.x + i * 24 * cos(act.angle), act.y + i * 24 * sin(act.angle), act.angle, 4, 4, 3, 3)
		end
	end
end

Boss[3].bullUpdate = function(act, dt)
	act.x = act.x + 300 * cos(act.dir) * dt
	act.y = act.y + 300 * sin(act.dir) * dt
	act.life = act.life - dt
	if act.life <= 0 then
		table.insert(activitiesRemove, act)
		return false
	end
	if math.sqrt((player.x - act.x)^2 + (player.y - act.y)^2) < player.radius then
		player.hp = player.hp - 20
		table.insert(activitiesRemove, act)
	end
	
end

Boss[3].bullDraw = function(act)
	gr.rectangle("fill", act.x - 4, act.y - 4, 8, 8)
end

Boss[3].shoot = function(act, dir)
	local a = {}
	a.actor = "a"
	a.x = act.x
	a.y = act.y
	a.dir = dir
	a.life = 1.5
	a.update = Boss[3].bullUpdate
	a.draw = Boss[3].bullDraw
	table.insert(activities, a)
end

function place_boss(n)
	BOSS = Boss[n].place(-100, -100)
	boss = 1
end
